﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" EnableEventValidation="false" ValidateRequest="false" AutoEventWireup="true" CodeBehind="HotelListing.aspx.cs" Inherits="Veerabook.HotelListing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    List of Hotels
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHeader" runat="server">
    <link href="assets/js/footable/css/footable.core.css?v=2-0-1" rel="stylesheet" type="text/css" />
    <link href="assets/js/footable/css/footable.standalone.css" rel="stylesheet" type="text/css" />
    <link href="assets/js/footable/css/footable-demos.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="assets/js/dataTable/lib/jquery.dataTables/css/DT_bootstrap.css" />
    <link rel="stylesheet" href="assets/js/dataTable/css/datatables.responsive.css" />
    
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.slider.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui-1.9.1.js"></script>
    <style>
        .content-wrap {
            margin: 0 0 35px 0;
        }
        #CPContent_lvDataPager1 .btn {
            margin: 3px !important;
        }
        .htlImg {
            height: 250px !important;
            width: 100% !important;
        }
        .htlList {
            min-height: 500px !important;
            height: 580px;
        }
        .social-follower {
            max-height: 80px !important;
        }
        .panel-body {
            text-overflow: ellipsis;
            overflow: hidden;
            max-height: 280px;
        }
        .viewBtn {
            position: absolute;
            bottom: 4.4%;
            left: 8%;
            
        }
        .firstRoom {
            font-size: 8pt;
            font-family: Avantgarde, TeX Gyre Adventor, URW Gothic L, sans-serif;

        }
        .body-nest{
            min-height:150px;
        }
                #amount {
            background-color:initial !important;
        }
        body #CPContent_ddlStarRatingFilter > .chechCat {
            
        }
        body #CPContent_ddlStarRatingFilter > .starCat {
            font-size: 20px !important;
            font-style: italic;
        }
        body #CPContent_ddlStarRatingFilter > .fa-star {
            font-size: 15px !important;
            font-style:italic !important;
            color:#ddcf34;
        }
        #CPContent_ddlStarRatingFilter{
            margin-top:10px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
<div class="row"></div>    
 <div  class="container-fluid">
                <div class="row">


                    <div>
                        <!-- BLANK PAGE-->

                        <div class="nest" id="Blank_PageClose">
                            <div class="title-alt">
                                <h6>
                                    Filters</h6>
                                <div class="titleClose">
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Blank_Page_Content">
                                        <span class="entypo-down-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Blank_Page_Content" style="display:none;">
                                <%-- Enter hotel name  --%>
                                <div class="form-group col-lg-6 col-sx-12">
                                    
                                            <label for="exampleInputEmail2" class="sr-only"></label>
                                            <input type="text" placeholder="Enter the hotel name " id="lblhotelname" runat="server" class="form-control" />
                                       

                            </div>

                                <%-- Enter Loctaion --%>
                               <div class="form-group col-lg-6 col-sx-12 ">
                                
                                            <label for="exampleInputEmail2" class="sr-only"></label>
                                            <input type="text" placeholder="Enter the location " runat="server" id="lblloc" class="form-control">
                                           </div>
                                 <br /> 
                                <br />
                                
                                <%-- START star rating --%>
                                <div class="col-lg-6">
                                    <ul id="ddlStarRatingFilter" class="btn btn-secondary" runat="server">
                                        <asp:CheckBox runat="server"  ClientIDMode="Static" ID="Star1" name="Stars" CssClass="checkCat" value="1" text="1" /><i class='fa fa-star'></i>&nbsp;
                                        <asp:CheckBox runat="server"  ClientIDMode="Static" ID="Star2" name="Stars" CssClass="checkCat" value="2" text="2"/><i class='fa fa-star'></i>&nbsp;
                                        <asp:CheckBox runat="server"  ClientIDMode="Static" ID="Star3" name="Stars" CssClass="checkCat" value="3" text="3"/><i class='fa fa-star'></i>&nbsp;
                                        <asp:CheckBox runat="server"  ClientIDMode="Static" ID="Star4" name="Stars" CssClass="checkCat" value="4" text="4"/><i class='fa fa-star'></i>&nbsp;
                                        <asp:CheckBox runat="server"  ClientIDMode="Static" ID="Star5" name="Stars" CssClass="checkCat" value="5" text="5"/><i class='fa fa-star'></i>
                                    </ul>
                                </div>


                                <%-- Price Range --%>
                                
                                <div class="col-lg-6 col-sx-12">
                                            <label for="amount">PRICE RANGE:</label> 
                                    <input type="text" id="amount" runat="server" style="border:0; color:#f6931f; font-weight:bold;" />
                                    <div id="slider-range"></div>
                                    
                                           </div> 
                                
                            <hr />
                                <br />
                            <div class="col-xs-12">
                                <asp:Button ID="btnApplyFilters" runat="server" CssClass="btn btn-success pull-right" OnClick="btnApplyFilters_Click" Text="Apply filters" />
                            </div>
                            </div>
                                
                        </div>
                    </div>
                    <!-- END OF BLANK PAGE -->



                </div>
                    </div>
    <br /> <br />
<div class="social-wrap">
    
    <asp:ListView ID="lvHotelsList" runat="server" GroupItemCount="4" GroupPlaceholderID="groupPlaceHolder1" ItemPlaceholderID="itemPlaceHolder1" OnPagePropertiesChanging="lvHotelsList_PagePropertiesChanging" OnItemDataBound="lvHotelsList_ItemDataBound">
        <EmptyDataTemplate>
        
        </EmptyDataTemplate>

        <LayoutTemplate>
            <div class="row">
                <asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
            </div>
        </LayoutTemplate>

        <GroupTemplate>
            <div class="row">
                <asp:PlaceHolder runat="server" ID="itemPlaceHolder1"></asp:PlaceHolder>
            </div>
        </GroupTemplate>

        <ItemTemplate>
            <div class="col-sm-3">
                <div class="panel panel-default htlList">
                    <div class="panel-facebook">
                       <asp:Image id="htlImg" runat="server" CssClass="img-responsive htlImg" AlternateText="Hotel Image"/>
                    </div>
                    <div class="panel-body">
                        <p class="lead"><%# Eval("Hotel_Name") %></p>
                        <p class="social-follower"><%# Eval("Hotel_Address") %></p>
                        <h5><div ID="hotelCat"><span style="display:none;"><%# Eval("Hotel_Category") %></span><asp:Literal runat="server" ID="Hotelstars"></asp:Literal>    Star/s </div></h5>
                        <p class="htlListFooter">
                           <asp:Repeater id="RoomsRpt" runat="server" >
                               <ItemTemplate>
                                   <div class="firstRoom">
                                       <div class="col-xs-12 text-center"><%#Eval("Room_Name") %></div>

                                       <div class="col-xs-12 text-center" style="display:none;"><%#Eval("Meal_Name") %></div>

                                       <div class="col-xs-12 text-center"><span style="color:#3b9c22; font-weight:bold;"><%#Eval("Room_TotalPrice") %></span> USD</div>
                                   </div>
                                   <div class="clearfix"></div>
                                   <br/>
                               </ItemTemplate>

                           </asp:Repeater>
          
                                
                            
                        </p>
                        
                    </div>
                    <div class="row">
                        <div class="text-center viewBtn">
                            <a  href="HotelDetailPages.aspx?Id=<%#Eval("Hotel_ID") %>" role="button" class=" bottom btn btn-primary"> View All Rooms</a>
                        </div>
                    </div>
                </div>

            </div>
        </ItemTemplate>
    </asp:ListView>  
    
    
    
    
    


    
    

</div>




    <div class="row">
        <asp:TextBox id="txtSearchHotel" runat="server" Visible="False" TextMode="MultiLine" Rows="4"> </asp:TextBox>
        <%-- Paging start --%>
        
        <div class="row col-sm-12 marginTop">
            <div class="text-center">
                <asp:DataPager ID="lvDataPager1" runat="server" PagedControlID="lvHotelsList" PageSize="8">
                    <Fields>
                        <asp:NextPreviousPagerField ShowFirstPageButton="true" ShowPreviousPageButton="true" ShowLastPageButton="false" ShowNextPageButton="false" ButtonCssClass="first btn" RenderNonBreakingSpacesBetweenControls="false" />
                        <asp:NumericPagerField CurrentPageLabelCssClass="current btn" NextPreviousButtonCssClass="next btn" NumericButtonCssClass="numeric btn" ButtonCount="5" NextPageText=">" PreviousPageText="<" RenderNonBreakingSpacesBetweenControls="false" />
                        <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false" ShowLastPageButton="true" ShowNextPageButton="true" ButtonCssClass="last btn" RenderNonBreakingSpacesBetweenControls="false" />
                    </Fields>
                </asp:DataPager>
            </div>
        </div>
        <%-- Paging end --%>
        

        
        
        
        </div>

    
    
    




    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
    <script>
        $(function () {
            $("#slider-range").slider({
                range: true,
                min: 0,
                max: 5000,
                values: [0, 5000],
                slide: function (event, ui) {
                    $("#CPContent_amount").val("$" + ui.values[0] + " - $" + ui.values[1]+ "!");
                }
            });
            $("#CPContent_amount").val("$" + $("#slider-range").slider("values", 0) +
                " - $" + $("#slider-range").slider("values", 1));
        });
    </script>
</asp:Content>
