﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.Xml.Linq;

namespace Veerabook
{
    public partial class HotelDetailPages : System.Web.UI.Page
    {
        private DataRow newRow;
        Xtools xtools = new Xtools();
        Multi multi = new Multi();
        protected void Page_Load(object sender, EventArgs e)
        {
            BindHotelLv();
            StoreHotelDetails();

        }

        void BindHotelLv()
        {

            string text = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "Cache/" + Session["SessionSearchToken"] + ".XML");

            StringReader theReader = new StringReader(text);
            DataSet originalDataSet = new DataSet("dataSet");
            originalDataSet.ReadXml(theReader);

            //Get Only the Data of This Hotel out of the Cached XML 
            DataView view = new DataView(originalDataSet.Tables[0]);
            view.RowFilter = "Hotel_ID = '" + Request.QueryString["ID"].ToString() + "'";
            Session["Hotel_ID"] = Request.QueryString["ID"].ToString();
            var dv = view.ToTable();

            //Get Basic Info into Literals and get slideshow images 
            Hotel_name.Text = lblHotelName.Text = dv.Rows[0]["Hotel_name"].ToString();
            Hotel_Address.Text = dv.Rows[0]["Hotel_Address"].ToString();
            Session["Hotel_Location_LAT"] = dv.Rows[0]["Hotel_Location_LAT"].ToString();
            Session["Hotel_Location_LNG"] = dv.Rows[0]["Hotel_Location_LNG"].ToString();

            

            //int hotelCatVal = int.Parse(dv.Rows[0]["Hotel_Category"].ToString());
            //string stars = "";
            //for (var i = 0; i < hotelCatVal; i++)
            //{
            //    stars += "<i class='fa fa-star'></i> ";
                
            //}
            //stars += " Star/s";
            //Hotel_Category.Text = stars;
            
            //Hotel_Category.Text = dv.Rows[0]["Hotel_Category"].ToString();

            //Slideshow
            var dt = new DataTable();
            dt = xtools.getHotelImages(Hotel_name.Text + " " + dv.Rows[0]["Hotel_City_Name"].ToString(), 7);

            //Set first slide 
            first_slide.Src = dt.Rows[0]["Contenturl"].ToString();
            Session["HotelImg"] = first_slide.Src.ToString();

            HotelImgslideshow.DataSource = dt;
            HotelImgslideshow.DataBind();


            //Get all Rooms of This Hotel Databound into the Rooms Repeater 
            lvRooms.DataSource = dv;
            lvRooms.DataBind();

        }


        void BindHotelItemLv()
        {
            string text = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "Cache/GetItemDetails/" + Request.QueryString["ID"].ToString() + "_Details.XML");
            Session["HotelIDAmenities"] = Request.QueryString["ID"].ToString();
            StringReader theReader = new StringReader(text);
            DataSet originalDataSet = new DataSet("dataSet");
            originalDataSet.ReadXml(theReader);
         //   lblLocation.Text = originalDataSet.Tables["HotelResult"].Rows[0]["Hotel_Location"].ToString();
         //   lblDescription.Text = originalDataSet.Tables["HotelResult"].Rows[0]["Hotel_Description"].ToString();
         //   lvAmenities.DataSource = originalDataSet.Tables["HotelResult"];
         //  lvAmenities.DataBind();

            //for (int i = 0; i < 5; i++)
            //{
            //    lblamin.Text = originalDataSet.Tables["HotelResult"].Rows[i]["Hotel_Amenities"].ToString();
            //}



        }

        void StoreHotelDetails() 
        {
            WebRequest requestItem = WebRequest.Create("http://api.multireisen.com/getitemdetails/");
            requestItem.Method = "POST";
            string postDataItem = "xml=" + "<MultireisenAPI_Request><RequestHeader><LoginName>admin@traveloo.club</LoginName><SessionId>"+ Session["SessionId"] + "</SessionId><SearchToken>"+ Session["SessionSearchToken"] + "</SearchToken><Language>en</Language></RequestHeader><GetItemDetails><Hotel><ItemID>"+ Request.QueryString["ID"].ToString() + "</ItemID></Hotel></GetItemDetails></MultireisenAPI_Request>";
            byte[] byteArrayItem = Encoding.UTF8.GetBytes(postDataItem);
            requestItem.ContentType = "application/x-www-form-urlencoded";
            requestItem.ContentLength = byteArrayItem.Length;
            Stream dataStreamItem = requestItem.GetRequestStream();
            dataStreamItem.Write(byteArrayItem, 0, byteArrayItem.Length);
            dataStreamItem.Close();
            WebResponse responseItem = requestItem.GetResponse();

            dataStreamItem = responseItem.GetResponseStream();
            StreamReader readerItem = new StreamReader(dataStreamItem);
            string responseFromServerItem = readerItem.ReadToEnd();

            StringReader theReaderItem = new StringReader(responseFromServerItem);
            DataSet OriginalDataSetItem = new DataSet("dataSet");

            OriginalDataSetItem.ReadXml(theReaderItem);


            //Write to Text File Caching
            System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "Cache/GetItemDetails/" + Request.QueryString["ID"].ToString() + ".XML", responseFromServerItem);

            //Split Rooms Using XML Nodes
            XmlDocument xdocItem = new XmlDocument();
            xdocItem.Load(AppDomain.CurrentDomain.BaseDirectory + "Cache/GetItemDetails/" + Request.QueryString["ID"].ToString() + ".XML");

            //  var xmls = xDoc.Root.Elements().ToArray(); // split into elements
            XmlNodeList listItem = xdocItem.SelectNodes("//HotelResult");

            DataTable dtItem = new DataTable();

            dtItem.Columns.Add("Hotel_Location");
            dtItem.Columns.Add("Hotel_Description");

            dtItem.Columns.Add("Hotel_Amenities");

            //DataSet ds = new DataSet();
            //ds.Tables.Add(dtItem);
            int numAmen;
            string nodeName;


            //for (int i = 0; i < listItem.Count; i++)
            //{
            //    DataRow newRow1 = dtItem.NewRow();
            //    newRow1["Hotel_Location"] = listItem[i].ChildNodes[0].ChildNodes[1].InnerText;
            //    newRow1["Hotel_Description"] = listItem[i].ChildNodes[0].ChildNodes[7].InnerText;

            //    dtItem.Rows.Add(newRow1);
            //}

            //for (int i = 0; i < 21; i++)
            //{
            //    DataRow newRow = dtItem.NewRow();
            //    numAmen = i + 10;
            //    nodeName = listItem[0].ChildNodes[0].ChildNodes[numAmen].Name.ToString();
            //    if (nodeName != "rooms_description")
            //    {
            //        break;
            //    }
            //    else
            //    {
            //        newRow["Hotel_Amenities"] = "<i class='fa fa-check-circle circle-icon' aria-hidden='true'> " + listItem[0].ChildNodes[0].ChildNodes[numAmen].ChildNodes[0].InnerText + "</i><br />";
            //        dtItem.Rows.Add(newRow);
            //    }
            //}



            //Cache the Over All Result to XML File 
            dtItem.TableName = "HotelResult";
            dtItem.WriteXml(AppDomain.CurrentDomain.BaseDirectory + "Cache/GetItemDetails/" + Request.QueryString["ID"].ToString() + "_Details.XML");




            BindHotelItemLv();


            readerItem.Close();
            dataStreamItem.Close();
            responseItem.Close();
        }

     

        protected void lvRooms_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            System.Data.DataRowView rowView = e.Item.DataItem as System.Data.DataRowView;

            string hotel_id = rowView["Hotel_ID"].ToString() ;
            string session_id = rowView["SessionId"].ToString();
            string room_id = rowView["Room_ID"].ToString();
            string search_token = rowView["SearchToken"].ToString();



            string RoomInfo = multi.GetRoomData(hotel_id, room_id, session_id, search_token);

            using (FileStream fs = File.Create(AppDomain.CurrentDomain.BaseDirectory + "Cache/RoomInfo_" + Session["SessionSearchToken"] + "_" + room_id + ".XML"))
            {
                byte[] info = new UTF8Encoding(true).GetBytes(RoomInfo);

                fs.Write(info, 0, info.Length);
            }



        }



    }
}