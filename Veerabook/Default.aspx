﻿<%@ Page Title="" Language="C#" Debug="true" EnableEventValidation="false" ValidateRequest="false" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Veerabook.Default" %>
<%@ Register TagPrefix="cc1" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=18.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%--  --%>
<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    Dashboard
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHeader" runat="server">
    <script src="https://code.jquery.com/jquery-1.11.2.js" ></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" ></script>
    <link  href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/js/datepicker/datepicker.css">
    <link rel="stylesheet" href="assets/js/datepicker/bootstrap-datetimepicker.css">
    <script type="text/javascript" src="assets/js/datepicker/bootstrap-datetimepicker.js"></script>
    
<%--    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>--%>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

    <style>
        .marginRightSm {           
            margin-right:8px;
        }
        .input-group[class*="col-"] {
            float: left !important;
        }
        .body-nest {
            min-height:400px !important;
        }
        #numRooms {
            margin-left:15px ;
        }
        .ddlListItem {
            float: none !important;
            width: 100%;
         
        }
        .ranges{
            display:none !important;
        }
        body .form-control {
            color: black !important;
            border: 1px solid #ccc !important;
            border-radius: 4px !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtDestinations').autocomplete({
                source: function (request, response)
                {
                    $.ajax({
                        url: 'Service.asmx/SearchCity',
                        method: 'POST',
                        contentType: 'application/json;charset-utf-8',
                        data: JSON.stringify({ prefixText: $('#txtDestinations').val(), count: 1 }),
                        dataType: 'json',
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (err) {
                            alert("No city with this name, please make sure of the spelling!");
                        }
                    });
                }
            });
        });
    </script>

   

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <%-- SEARCH Element --%>
          
    <div class="row">
        <div class="col-sm-12">
            <div class="nest" id="inlineClose">
                <div class="title-alt">
                    <h6>
                        Find Your Destination</h6>
                    <div class="titleClose">
                                    
                    </div>
                    <div class="titleToggle">
                        <a class="nav-toggle-alt" href="#inline">
                            <span class="entypo-up-open"></span>
                        </a>
                    </div>

                </div>

                <div class="body-nest" id="inline">

                    <div class="form_center  col-sm-12">
                        <div class="form-inline row">
                            <asp:TextBox id="xresult" rows="2" cols="20" style="width:100%" runat="server"></asp:TextBox>
                            <div class="form-group">
                                            
                                           
                                                 
                                <asp:TextBox runat="server" ClientIDMode="Static" id="txtDestinations" class="form-control col-sm-2 marginRightSm" placeholder="Your Destinations" ></asp:TextBox>

                                <div id="demo" class="col-sm-4 input-group date marginRightSm">
                                    <asp:TextBox id="startDate" runat="server" class="form-control" placeholder="Check In &amp; out "></asp:TextBox>
                                    <asp:TextBox  id="chckIn" runat="server" class="form-control" style="display: none !important;" ></asp:TextBox>
                                    <asp:TextBox  id="chckOut" runat="server" class="form-control" style="display: none !important;" ></asp:TextBox>
                                    <span class="input-group-addon add-on">
                                        <i style="font-style:normal;" data-time-icon="entypo-clock" data-date-icon="entypo-calendar" class="entypo-calendar"></i>
                                    </span>
                                </div>


                                <div class="col-sm-3 input-group marginRightSm">
                                    <asp:DropDownList ID="ddlNoRooms" AutoPostBack="true" OnSelectedIndexChanged="ddlNoRooms_SelectedIndexChanged" CssClass="btn btn-secondary col-sm-3 form-control" runat="server">
                                        <asp:ListItem Text="Please Select Rooms" Value="-1" />
                                        <asp:ListItem Text="1" Value="1" />
                                        <asp:ListItem Text="2" Value="2" />
                                        <asp:ListItem Text="3" Value="3" />
                                        <asp:ListItem Text="4" Value="4" />
                                        <asp:ListItem Text="5" Value="5" />
                                        <asp:ListItem Text="6" Value="6" />
                                    </asp:DropDownList>
                                </div>  
                                <div class="col-sm-2">                                          
                                    <asp:Button runat="server" id="btnSearchHotel" OnClick="btnSearchHotel_Click" Text="Search" class="btn btn-success btnSearchHotel" />
                                </div>
                                <div class="clearfix"></div>
                                <%--<asp:Panel ID="pnlRooms" CssClass="row marginTop" runat="server">
                                </asp:Panel>--%>
                                <asp:ListView ID="lvRooms" runat="server">
                                    <ItemTemplate>
                                        <asp:Panel ID="pnlRoom" CssClass="row col-xs-12 marginTopSm" runat="server">
                                            <asp:TextBox ID="adltNum"    placeholder="NumberOfAdult" CssClass="input-group form-control marginRight" type="number" runat="server" />
                                            <asp:TextBox ID="childNum"   placeholder="NumberOfChildren" CausesValidation="true" CssClass="input-group form-control marginRight" OnTextChanged="childNum_TextChanged" AutoPostBack="true" type="number" runat="server" />
                                            <asp:TextBox ID="childAge1"  placeholder="AgeChild1" CssClass="input-group form-control marginRight" Enabled="false" type="number" runat="server" />
                                            <asp:TextBox ID="childAge2"  placeholder="AgeChild2" CssClass="input-group form-control marginRight" Enabled="false" type="number" runat="server" />
                                            <asp:TextBox ID="childAge3"  placeholder="AgeChild3" CssClass="input-group form-control" Enabled="false" type="number" runat="server" />

                                        </asp:Panel>
                                    </ItemTemplate>
                                </asp:ListView>
                                
                    
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
               
    <%-- END SEARCH ELEMENT --%>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">

    <script type="text/javascript">
        var checkIn = "";
        var checkOut = "";
        var today = new Date();
        Date.prototype.addDays = function (days) {
            var dat = new Date(this.valueOf());
            dat.setDate(dat.getDate() + days);
            return dat;
        }
        //var txtdates = "From " + today + " to " + today.addDays(1);
        //$("[name='ctl00$CPContent$startDate']").val(txtdates);
        //$("[name='ctl00$CPContent$chckIn']").val(today);
        //$("[name='ctl00$CPContent$chckOut']").val(today.addDays(1));

        $('#demo').daterangepicker({
            "startDate": today,
            "endDate": today.addDays(1)
        }, function (start, end, label) {
            console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
            checkIn = start.format('YYYY-MM-DD');
            checkOut = end.format('YYYY-MM-DD');
            var dates = "From " + checkIn + " To " + checkOut ;
            $("[name='ctl00$CPContent$startDate']").val(dates);
            $("[name='ctl00$CPContent$chckIn']").val(checkIn);
            $("[name='ctl00$CPContent$chckOut']").val(checkOut);
            
            //chckIn


        });

    </script>

    
<%--    <script type="text/javascript">
                    
        $(function() {
            function log( message ) {
                $( "<div>" ).text( message ).prependTo( "#log" );
                $( "#log" ).scrollTop( 0 );
            }

            $( "#txtDestinations" ).autocomplete({
                source: function( request, response ) {
                    $.ajax({
                        type: "POST",
                        url: "Service.asmx/SearchCity",
                        data: JSON.stringify({ prefixText: prefixTextStr, count: 1 }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            alert(msg.d);
                        },
                        error: function (msg) {
                            alert(msg.d);
                        }
                    });
                },
                minLength: 3,
                select: function( event, ui ) {
                    log( ui.item ?
                        "Selected: " + ui.item.label :
                        "Nothing selected, input was " + this.value);
                },
                open: function() {
                    $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
                },
                close: function() {
                    $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
                }
            });
        });
    </script>--%>
</asp:Content>
