﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Veerabook
{
    public partial class GetRemarks : System.Web.UI.Page
    {

        Multi multi = new Multi();


        protected void Page_Load(object sender, EventArgs e)
        {
            string hote_lid = Request.QueryString["hotelid"];
            string room_id = Request.QueryString["roomid"];
            string session_id = Request.QueryString["sessionid"];
            string search_token = Request.QueryString["searchtoken"];

          

            Result.Text = multi.GetRoomData(hote_lid, room_id, session_id, search_token);
        }
    }
}