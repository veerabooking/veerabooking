﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using System.Xml;

namespace Veerabook
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        private Xtools xtools = new Xtools();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void login()
        {
            WebRequest request = WebRequest.Create("http://api-test.multireisen.com/login/");
            request.Method = "POST";
            string postData = "xml=" + "<MultireisenAPI_Request><Login><LoginName>admin@dynawix.com</LoginName><Passw>123</Passw></Login></MultireisenAPI_Request>";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse response = request.GetResponse();

            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            xresult.Text = responseFromServer;
            StringReader theReader = new StringReader(xresult.Text);
            DataSet OriginalDataSet = new DataSet("dataSet");

            OriginalDataSet.ReadXml(theReader);
            //grid1.DataSource = OriginalDataSet.Tables["Login"];
            //grid1.DataBind();

            Session["SessionId"] = OriginalDataSet.Tables["Login"].Rows[0]["SessionID"];




            reader.Close();
            dataStream.Close();
            response.Close();

        }

        protected void btnLogin_OnClick(object sender, EventArgs e)
        {
            login();
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            SearchToken();
            GetHotelData();

        }

        void SearchToken()
        {
            WebRequest request = WebRequest.Create("http://api-test.multireisen.com/hotelsearch/");
            request.Method = "POST";
            string postData = "xml=" + "<MultireisenAPI_Request><RequestHeader><LoginName>admin@dynawix.com</LoginName><SessionId>" + Session["SessionId"].ToString() + "</SessionId><Language>en</Language><AsyncMode>FALSE</AsyncMode></RequestHeader><SearchData><Hotel><Destination><CityCode></CityCode><CityName>Hurghada</CityName></Destination><CheckIn>2018-09-01</CheckIn><CheckOut>2018-09-03</CheckOut><Rooms><Room><RoomType>2</RoomType><Children></Children></Room></Rooms><Category></Category></Hotel></SearchData></MultireisenAPI_Request>";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse response = request.GetResponse();

            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            txtSearchHotel.Text = responseFromServer;
            StringReader theReader = new StringReader(txtSearchHotel.Text);
            DataSet OriginalDataSet = new DataSet("dataSet");
            OriginalDataSet.ReadXmlSchema(AppDomain.CurrentDomain.BaseDirectory + "XML/Schema/MR.XSD");
            OriginalDataSet.ReadXml(theReader);
            Session["SessionToken"] = OriginalDataSet.Tables[0].Rows[0]["SearchToken"];

            //gvHotels.DataSource = OriginalDataSet.Tables[0];
            //gvHotels.DataBind();


            reader.Close();
            dataStream.Close();
            response.Close();
        }

        void GetHotelData()
        {
            WebRequest request = WebRequest.Create("http://api-test.multireisen.com/hotelsearch/result/");
            request.Method = "POST";
            string postData = "xml=" + "<MultireisenAPI_Request><RequestHeader><LoginName>admin@dynawix.com</LoginName><SessionId>" + Session["SessionId"].ToString() + "</SessionId><SearchToken>" + Session["SessionToken"].ToString() + "</SearchToken></RequestHeader></MultireisenAPI_Request>";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse response = request.GetResponse();

            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            txtSearchHotel.Text = responseFromServer;
            StringReader theReader = new StringReader(txtSearchHotel.Text);
            DataSet OriginalDataSet = new DataSet("dataSet");
            OriginalDataSet.ReadXmlSchema(AppDomain.CurrentDomain.BaseDirectory + "XML/Schema/MR.XSD");
            OriginalDataSet.ReadXml(theReader);
            //Session["SessionToken"] = OriginalDataSet.Tables[0].Rows[0]["SearchToken"];

            gvHotels.DataSource = OriginalDataSet.Tables["HotelResults"];
            gvHotels.DataBind();


            reader.Close();
            dataStream.Close();
            response.Close();
        }
        protected DataSet book()
        {
            WebRequest request = WebRequest.Create("http://api-test.multireisen.com/initbooking/");
            request.Method = "POST";
            string postData = "xml=" + "<MultireisenAPI_Request><RequestHeader><LoginName>admin@dynawix.com</LoginName><SessionId>4uffsvi4o0asb5k710i7sg0nv6</SessionId><SearchToken>79ae3d4b8ce84e5f450c78583d3c8817</SearchToken></RequestHeader><InitBookingData><Contact><Title>Mr</Title><Lastname>Teszt</Lastname><Firstname>Contact</Firstname><Email>test.user@gmail.com</Email><Phone>123456789</Phone><Country>HU</Country><Zip>1234</Zip><City>Budapest</City><Address>Teszt utca 11</Address></Contact><Invoice><Name> Test Company </Name><Country>HU</Country><Zip>1234</Zip><City>Budapest</City><Address>Teszt utca 11 </Address></Invoice><Passengers><Passenger><Type>ADT</Type><Title>Mr</Title><Firstname>John</Firstname><Lastname>Doe</Lastname><Birthdate>1971-05-13</Birthdate><Nationality>Egyptian</Nationality><PassportNumber>4545</PassportNumber><PassportCountry>Egypt</PassportCountry><PassportIssued>05/17</PassportIssued><PassportExp>05/22</PassportExp></Passenger><Passenger><Type>ADT</Type><Title>Mr</Title><Firstname>John</Firstname><Lastname>Doe</Lastname><Birthdate>1971-05-13</Birthdate><Nationality>Egyptian</Nationality><PassportNumber>4545</PassportNumber><PassportCountry>Egypt</PassportCountry><PassportIssued>05/17</PassportIssued><PassportExp>05/22</PassportExp></Passenger></Passengers><Paymode>9</Paymode></InitBookingData></MultireisenAPI_Request>";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse response = request.GetResponse();

            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            xresult.Text = responseFromServer;
            StringReader theReader = new StringReader(xresult.Text);
            DataSet OriginalDataSet = new DataSet("dataSet");

            OriginalDataSet.ReadXml(theReader);
            return OriginalDataSet;

        }

        protected void btnInit_Click(object sender, EventArgs e)
        {
            WebRequest requestItem = WebRequest.Create("http://api-test.multireisen.com/initbooking/");
            requestItem.Method = "POST";
            string postDataItem = "xml=" + "<MultireisenAPI_Request><RequestHeader><LoginName>admin@dynawix.com</LoginName><SessionId>r74qj7tpl5bc58anlfmalrl0b2</SessionId><SearchToken>d5caefbcea268b812cbcd6772b6fa45c</SearchToken></RequestHeader><InitBookingData><Contact><Title>Mr</Title><Lastname>Teszt</Lastname><Firstname>Contact</Firstname><Email>test.user@gmail.com</Email><Phone>123456789</Phone><Country>HU</Country><Zip>1234</Zip><City>Budapest</City><Address>Teszt utca 11</Address></Contact><Invoice><Name> Test Company </Name><Country>HU</Country><Zip>1234</Zip><City>Budapest</City><Address>Teszt utca 11 </Address></Invoice><Passengers><Passenger><Type>ADT</Type><Title>Mr</Title><Firstname>John</Firstname><Lastname>Doe</Lastname><Birthdate>1971-05-13</Birthdate><Nationality>Egyptian</Nationality><PassportNumber>4545</PassportNumber><PassportCountry>Egypt</PassportCountry><PassportIssued>05/17</PassportIssued><PassportExp>05/22</PassportExp></Passenger><Passenger><Type>ADT</Type><Title>Mr</Title><Firstname>John</Firstname><Lastname>Doe</Lastname><Birthdate>1971-05-13</Birthdate><Nationality>Egyptian</Nationality><PassportNumber>4545</PassportNumber><PassportCountry>Egypt</PassportCountry><PassportIssued>05/17</PassportIssued><PassportExp>05/22</PassportExp></Passenger></Passengers><Paymode>9</Paymode></InitBookingData></MultireisenAPI_Request>";
            byte[] byteArrayItem = Encoding.UTF8.GetBytes(postDataItem);
            requestItem.ContentType = "application/x-www-form-urlencoded";
            requestItem.ContentLength = byteArrayItem.Length;
            Stream dataStreamItem = requestItem.GetRequestStream();
            dataStreamItem.Write(byteArrayItem, 0, byteArrayItem.Length);
            dataStreamItem.Close();
            WebResponse responseItem = requestItem.GetResponse();

            dataStreamItem = responseItem.GetResponseStream();
            StreamReader readerItem = new StreamReader(dataStreamItem);
            string responseFromServerItem = readerItem.ReadToEnd();

            StringReader theReaderItem = new StringReader(responseFromServerItem);
            DataSet OriginalDataSetItem = new DataSet("dataSet");

            OriginalDataSetItem.ReadXml(theReaderItem);



            //Write to Text File Caching
            System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "Cache/GetItemDetails/HotelInitBooking.XML", responseFromServerItem);

            //Split Rooms Using XML Nodes
            XmlDocument xdocItem = new XmlDocument();
            xdocItem.Load(AppDomain.CurrentDomain.BaseDirectory + "Cache/GetItemDetails/HotelInitBooking.XML");

            //  var xmls = xDoc.Root.Elements().ToArray(); // split into elements
            XmlNodeList listItem = xdocItem.SelectNodes("//InitBooking");

            DataTable dtItem = new DataTable();

            dtItem.Columns.Add("Hotel_ID");
            dtItem.Columns.Add("bookingitemid");

            dtItem.Columns.Add("total_Price");
            dtItem.Columns.Add("BookingId");
            string SessionId = "r74qj7tpl5bc58anlfmalrl0b2";
            string SearchToken = "d5caefbcea268b812cbcd6772b6fa45c";

            for (int i = 0; i < listItem.Count; i++)
            {
                DataRow newRow1 = dtItem.NewRow();
                newRow1["Hotel_ID"] = listItem[i].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].InnerText;
                newRow1["bookingitemid"] = listItem[i].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[4].InnerText;
                newRow1["total_Price"] = listItem[i].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[6].ChildNodes[5].InnerText;
                newRow1["BookingId"] = listItem[i].ChildNodes[2].InnerText;
                //
                string sqlQuery = "INSERT INTO [dbo].[Hotel_InitBooking] (Hotel_ID, bookingitemid, total_Price, BookingId, SessionId, SearchToken) VALUES (" + newRow1["Hotel_ID"].ToString() + "," + newRow1["bookingitemid"].ToString() + "," + decimal.Parse(newRow1["total_Price"].ToString()) + "," + newRow1["BookingId"].ToString() + "," + SessionId + "," + SearchToken + ")";
                xtools.SQLINSERT(sqlQuery);
                dtItem.Rows.Add(newRow1);
            }
            grid1.DataSource = dtItem;
            grid1.DataBind();
        }
    }
}