﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="sliderform.aspx.cs" Inherits="Veerabook.sliderform" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHeader" runat="server">
    <style>
        #amount {
            background-color:initial !important;
        }
    </style>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.slider.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui-1.9.1.js"></script>
<%--    <script>
        $(document).ready(function () {
            $("#ddlStarRatingFilter > li").click(function () {
                var label = $(this > label).val();
                alert(label);
            });
        });
    </script>--%>
    <style>
        body #CPContent_ddlStarRatingFilter > .chechCat {
            
        }
        body #CPContent_ddlStarRatingFilter > .starCat {
            font-size: 20px !important;
            font-style: italic;
        }
        body #CPContent_ddlStarRatingFilter > .fa-star {
            font-size: 15px !important;
            font-style:italic !important;
        }


    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <p>
        <label for="amount">Price range:</label>
        <input type="text" id="amount" style="border:0; color:#f6931f; font-weight:bold;" />
    </p>
    <div id="slider-range"></div>
    <hr />
    <br />
<%--    <%-- START star rating --%>
<%---    <ul id="ddlStarRatingFilter" class="btn btn-default dropdown-toggle" runat="server">
        <input type="checkbox" class="checkCat" value="1" /><label class="starCat">1 </label><i class='fa fa-star'></i>&nbsp;
        <input type="checkbox" class="checkCat" value="2"/><label class="starCat">2 </label><i class='fa fa-star'></i>&nbsp;
        <input type="checkbox" class="checkCat" value="3"/><label class="starCat">3 </label><i class='fa fa-star'></i>&nbsp;
        <input type="checkbox" class="checkCat" value="4"/><label class="starCat">4 </label><i class='fa fa-star'></i>&nbsp;
        <input type="checkbox" class="checkCat" value="5"/><label class="starCat">5 </label><i class='fa fa-star'></i>
    </ul>--%>

    <%-- Start Star rating by drop down list --%>
<%--    <asp:DropDownList ID="ddlStarRatingFilter" CssClass="btn btn-default dropdown-toggle col-md-2" runat="server">
        <asp:ListItem Text="1 Star" Value="1"></asp:ListItem>
        <asp:ListItem Text="2 Stars" Value="2"></asp:ListItem>
        <asp:ListItem Text="3 Stars" Value="3"></asp:ListItem>
        <asp:ListItem Text="4 Stars" Value="4"></asp:ListItem>
        <asp:ListItem Text="5 Stars" Value="5"></asp:ListItem>
    </asp:DropDownList>--%>
    <%-- End Star rating by drop down list --%>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
    <script>
        $(function () {
            $("#slider-range").slider({
                range: true,
                min: 50,
                max: 500,
                values: [75, 300],
                slide: function (event, ui) {
                    $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
                }
            });
            $("#amount").val("$" + $("#slider-range").slider("values", 0) +
                " - $" + $("#slider-range").slider("values", 1));
        });
    </script>

</asp:Content>
