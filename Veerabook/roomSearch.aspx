﻿<%@ Page Language="C#" CodeBehind="roomSearch.aspx.cs" Inherits="Veerabook.roomSearch" AutoEventWireup="true" Debug="true" EnableEventValidation="false" ValidateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
</head>
<body>
    <form runat="server">
        <div class="container-fluid">
            <%--<asp:Panel ID="pnlRoom" CssClass="row col-xs-12 marginTopSm" runat="server">--%>
            <asp:TextBox ID="adltNum" placeholder="NumberOfAdult" CssClass="input-group form-control marginRight" type="number" runat="server" />
            <asp:TextBox ID="childNum" placeholder="NumberOfChildren" CssClass="input-group form-control marginRight childNum" type="number" runat="server" />
            <asp:TextBox ID="childAge1" placeholder="AgeChild1" CssClass="input-group form-control marginRight" Enabled="false" type="number" runat="server" />
            <asp:TextBox ID="childAge2" placeholder="AgeChild2" CssClass="input-group form-control marginRight" Enabled="false" type="number" runat="server" />
            <asp:TextBox ID="childAge3" placeholder="AgeChild3" CssClass="input-group form-control" Enabled="false" type="number" runat="server" />

            <%--</asp:Panel>--%>
        </div>
        <script>
            //$(document).ready();
            //roomData1
            for (var i = 0; i < 6; i++) {
                var roomNum = "#roomData" + (i + 1);
                $(roomNum + " input#childNum").change(function () {
                    var txtBox = $(this);
                    var parentContainer = $(txtBox).parent().parent().parent();
                    var prntID = parentContainer[0].id;
                    //alert(prntID);
                    
                    var KidNum = txtBox.val();
                    //disable all rooms first
                    for (var y = 0; y < 3 ; y++) {
                        var Ordery = "#" + prntID + " form div #childAge" + (y + 1);
                        $(Ordery).val('');
                        $(Ordery).attr("disabled", "disabled");
                        
                    }
                    //then enable rooms depending on children count
                    for (var x = 0; x < parseInt(KidNum) ; x++) {
                        var kidOrder = "#" + prntID + " form div #childAge" + (x + 1);
                        $(kidOrder).removeAttr("disabled");
                    }

                });
            }

        </script>
    </form>
</body>
</html>
