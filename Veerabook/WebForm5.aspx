﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" EnableEventValidation="false" ValidateRequest="false" CodeBehind="WebForm5.aspx.cs" Inherits="Veerabook.WebForm5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    Trial search
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHeader" runat="server">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <script src="https://code.jquery.com/jquery-1.11.2.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/js/datepicker/datepicker.css">
    <link rel="stylesheet" href="assets/js/datepicker/bootstrap-datetimepicker.css">

    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <!-- Include Date Range Picker -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css">

    <style>
        .marginRightSm {
            margin-right: 8px;
        }

        .input-group[class*="col-"] {
            float: left !important;
        }

        .body-nest {
            min-height: 400px !important;
        }

        #numRooms {
            margin-left: 15px;
        }

        .ddlListItem {
            float: none !important;
            width: 100%;
        }

        .ranges {
            display: none !important;
        }

        body .form-control {
            color: black !important;
            border: 1px solid #ccc !important;
            border-radius: 4px !important;
        }

        .title-alt {
            margin: unset !important;
        }

        #inline col-sm-12 {
            margin-left: 10px;
        }

        div[id*="roomData"] {
            margin-top: 20px;
        }

        #inlineClose {
            min-height: 1000px;
        }
    </style>
    
    <link href="assets/css/entypo-icon.css" rel="stylesheet">
    
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <script type="text/javascript" src="assets/js/datepicker/bootstrap-datetimepicker.js"></script>

    <link href="css/searchLoader.css" rel="stylesheet" />
    <style>
        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        .loader {
            border: 16px solid #f3f3f3;
            border-top: 16px solid #3498db;
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            position: fixed;
            top: 50%;
            left: 45%;
            background-color: #fff;
        }

        .loaderCont {
            overflow: hidden;
            height: 1500px;
            width: 100%;
            background-color: #000;
            position: fixed;
            top: 0;
            z-index: 5000;
            left: 0;
        }
    </style>
    <script>
        $("#btnSearchHotel").click(function () {
            $(document).ajaxStart(function () {
                $(".loaderCont").css("display", "block");
                checkIframeLoaded();

            });
            $(document).ajaxComplete(function () {
                $(".loaderCont").css("display", "none");
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <div class="row">
        <%--        <iframe src="searchPage.aspx" scrolling="no" class="iframeDesign" id="i_frame"></iframe>--%>
        <div id="loadSearchBar"></div>
        <div class="clearfix"></div>
        <div id="roomData1"></div>
        <div id="roomData2"></div>
        <div id="roomData3"></div>
        <div id="roomData4"></div>
        <div id="roomData5"></div>
        <div id="roomData6"></div>
    </div>
    <div class="loaderCont" style="display: none;">
        <div class="loader"></div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">

    <script>
        //function checkIframeLoaded() {
        //    // Get a handle to the iframe element
        //    var iframe = document.getElementById('i_frame');
        //    var iframeDoc = iframe.contentDocument || iframe.contentWindow.document;

        //    // Check if loading is complete
        //    if (iframeDoc.readyState == 'complete') {
        //        //iframe.contentWindow.alert("Hello");
        //        iframe.contentWindow.onload = function () {
        //            alert("I am loaded");
        //        };
        //        // The loading is complete, call the function we want executed once the iframe is loaded
        //        afterLoading();
        //        return;
        //    }

        //    // If we are here, it is not loaded. Set things up so we check   the status again in 100 milliseconds
        //    window.setTimeout(checkIframeLoaded, 100);
        //}

        //function afterLoading() {
        //    alert("I am here");
        //}
    </script>

    <script>
        $(document).ready(function () {
            $("#loadSearchBar").load("searchPage.aspx");
        });
    </script>
     <script>
            //$(document).ready(
            //    setInterval(function () {
            //        var roomCount = $('#ddlNoRooms :selected').text();
            //        alert(roomCount);
            //        $("#roomData").load("images.aspx");

            //    }, 30000)
            //);
            $("#ddlNoRooms").change(function () {
                var roomCount = $('#ddlNoRooms :selected').text();
                for (var x = 0; x < 6; x++) {
                    $('#roomData' + (x + 1)).empty();
                }
                for (var i = 0; i < parseInt(roomCount) ; i++) {

                    $('#roomData' + (i + 1)).load("roomSearch.aspx");
                }
            });
            $("#btnSearchHotel").click(function () {
                roomData();
                //alert("added");

            });
            function roomData() {
                //fixed variables
                var searchData = "";
                var roomStr = "";
                var roomsStr;

                var destination = $("#txtDestinations").val();

                var checkIn = $("#chckIn").val();

                var checkOut = $("#chckOut").val();

                var roomsNum = $('#ddlNoRooms :selected').text();

                for (var i = 0; i < parseInt(roomsNum) ; i++) {
                    var txtChldStr = "";
                    var roomID = "#roomData" + (i + 1);
                    var adltCount = $(roomID + " form div #adltNum").val();
                    //var AdltCount = " Room" + (i + 1) + " = " + adltCount + " , ";

                    var txtAdltStr = "<RoomType>" + adltCount + "</RoomType>";

                    var Kids = $(roomID + " form div #childNum").val();
                    var txtChldAge = "";
                    if (parseInt(Kids) > 0) {
                        for (var z = 0; z < Kids; z++) {
                            var KidOrdr = "#childAge" + (z + 1);
                            var kidAge = $(roomID + " form div " + KidOrdr).val();

                            txtChldAge += "<Age>" + kidAge + "</Age>";

                        }
                        txtChldStr = "<Children>" + txtChldAge + "</Children>";
                    }

                    roomStr = roomStr + "<Room>" + txtAdltStr + txtChldStr + "</Room>";

                }
                roomsStr = "<Rooms>" + roomStr + "</Rooms>";


                document.cookie = "searchDataKoky=" + roomsStr;
            }
        </script>
</asp:Content>

