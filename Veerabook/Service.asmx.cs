﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace Veerabook
{
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class Service : WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [System.Web.Script.Services.ScriptMethod()]
        [WebMethod]
        public  List<string> SearchCity(string prefixText, int count)
        {


            List<string> customers = new List<string>();

            Xtools xtool = new Xtools();


            DataTable dtx = xtool.SQLREAD("SELECT Distinct CITY_NAME_EN FROM Cities where city_name_En LIKE '%" + prefixText +  "%'").Tables[0];


            foreach (DataRow row in dtx.Rows)
            {
                customers.Add(row["City_name_en"].ToString());
            }
           
           


            return customers;
        }
    }
}
