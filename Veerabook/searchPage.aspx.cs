﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Veerabook
{
    public partial class searchPage : System.Web.UI.Page
    {
        Xtools xtools = new Xtools();
        Multi multi = new Multi();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                string today = DateTime.Now.ToString("yyyy-MM-dd");
                DateTime endDate = Convert.ToDateTime(today);
                Int64 addedDays = Convert.ToInt64(2);
                endDate = endDate.AddDays(addedDays);
                string end = endDate.ToString("yyyy-MM-dd");
                startDate.Text = "From " + today + " To " + end;
                chckIn.Text = today;
                chckOut.Text = end;
            }
        }
        protected void ddlNoRooms_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("dt");
            dt.Columns.Add("ID");
            for (int i = 0; i < int.Parse(ddlNoRooms.SelectedValue.ToString()); i++)
            {
                // TRY LISTVIEW TO LOOP ON ROOM DATA
                DataRow dr = dt.NewRow();
                dr[0] = i.ToString();
                dt.Rows.Add(dr);
            }

            ds.Tables.Add(dt);
            //lvRooms.DataSource = ds;
            //lvRooms.DataBind();
            Session["sessionNumRooms"] = ddlNoRooms.SelectedValue.ToString();
        }
        protected void btnSearchHotel_Click(object sender, EventArgs e)
        {
            login();
            searchDatas();

            // xresult.Text = Session["htlSrchPostData"].ToString() ;
            Response.Redirect("HotelListing.aspx");
            //Server.Transfer("HotelListing.aspx", true);
        }
        private void searchDatas()
        {
            HttpCookie searchDataKokies = Request.Cookies["searchDataKoky"];
            string myRoomData = searchDataKokies.Value;

            string postData = "<RequestHeader><LoginName>" + multi.LoginName + "</LoginName><SessionId>" + Session["SessionId"].ToString() + "</SessionId><Language>en</Language><AsyncMode>FALSE</AsyncMode></RequestHeader><SearchData><Hotel><Destination><CityCode></CityCode><CityName>" + txtDestinations.Text + "</CityName></Destination><CheckIn>" + chckIn.Text + "</CheckIn><CheckOut>" + chckOut.Text + "</CheckOut>" + myRoomData + "<Category></Category></Hotel></SearchData>";
            //Session["sessionNumAdlts"] = AdltCount;
            Session["htlSrchPostData"] = postData;
            Session["checkIn"] = chckIn.Text;
            Session["checkOut"] = chckOut.Text;
        }
        
        protected void login()
        {
            Session["SessionId"] = multi.GetLoginsession();

        }
    }
}