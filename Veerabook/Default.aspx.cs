﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;


namespace Veerabook
{



    public partial class Default : System.Web.UI.Page
    {

        Xtools xtools = new Xtools();
        Multi multi = new Multi();


        protected string roomStr;
        protected string roomsStr;
        protected string txtChldAge;
        protected string txtChldStr;
        protected string txtAdltStr;
        protected string AdltCount;
        private object childAge1;

        protected void Page_Load(object sender, EventArgs e)
        {
          

            if (!IsPostBack)
            {

                string today = DateTime.Now.ToString("yyyy-MM-dd");
                DateTime endDate = Convert.ToDateTime(today);
                Int64 addedDays = Convert.ToInt64(2);
                endDate = endDate.AddDays(addedDays);
                string end = endDate.ToString("yyyy-MM-dd");
                startDate.Text = "From " + today + " To " + end;
                chckIn.Text = today;
                chckOut.Text = end;
            }
        }

        protected void login()
        {

            Session["SessionId"] = multi.GetLoginsession();

        }

        protected void btnSearchHotel_Click(object sender, EventArgs e)
        {
            login();
            searchData();

            Response.Redirect("HotelListing.aspx");

        }

        private void searchData()
        {
            ListView newLvRooms = (ListView)lvRooms;
            int x = 0;
            foreach (ListViewItem item in newLvRooms.Items)
            {

                //ListViewItem pnlRoom = newLvRooms.FindControl("pnlRoom") as ListViewItem;
                ListViewItem pnlRoom = newLvRooms.Items[x] as ListViewItem;

                TextBox txtAdultNum = (TextBox)pnlRoom.FindControl("adltNum");
                AdltCount += " Room" + (x + 1) + " = " + txtAdultNum.Text + " , ";
                txtAdltStr = "<RoomType>" + txtAdultNum.Text + "</RoomType>";
                TextBox txtChildNum = (TextBox)pnlRoom.FindControl("childNum");
                txtChldAge = null;
                if (int.Parse(txtChildNum.Text) > 0)
                {
                    for (int i = 0; i < int.Parse(txtChildNum.Text); i++)
                    {
                        string chldAgeID = "childAge" + (i + 1);
                        TextBox txtChildAge = (TextBox)pnlRoom.FindControl(chldAgeID);
                        txtChldAge += "<Age>" + txtChildAge.Text + "</Age>";
                    }
                    txtChldStr = "<Children>" + txtChldAge + "</Children>";
                }
                roomStr += "<Room>" + txtAdltStr + txtChldStr + "</Room>";
                x++;
            }
            roomsStr = "<Rooms>" + roomStr + "</Rooms>";

            string postData ="<RequestHeader><LoginName>" + multi.LoginName + "</LoginName><SessionId>" + Session["SessionId"].ToString() + "</SessionId><Language>en</Language><AsyncMode>FALSE</AsyncMode></RequestHeader><SearchData><Hotel><Destination><CityCode></CityCode><CityName>" + txtDestinations.Text + "</CityName></Destination><CheckIn>" + chckIn.Text + "</CheckIn><CheckOut>" + chckOut.Text + "</CheckOut>" + roomsStr + "<Category></Category></Hotel></SearchData>";
            Session["sessionNumAdlts"] = AdltCount;
            Session["htlSrchPostData"] = postData;
            Session["checkIn"] = chckIn.Text;
            Session["checkOut"] = chckOut.Text;
        }

        protected void ddlNoRooms_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("dt");
            dt.Columns.Add("ID");
            for (int i = 0; i < int.Parse(ddlNoRooms.SelectedValue.ToString()); i++)
            {
                // TRY LISTVIEW TO LOOP ON ROOM DATA
                DataRow dr = dt.NewRow();
                dr[0] = i.ToString();
                dt.Rows.Add(dr);
            }

            ds.Tables.Add(dt);
            lvRooms.DataSource = ds;
            lvRooms.DataBind();
            Session["sessionNumRooms"] = ddlNoRooms.SelectedValue.ToString();
        }



      
        protected void fillRoomStr()
        {

        }

        protected void startDate_TextChanged(object sender, EventArgs e)
        {

        }
        protected void childNum_TextChanged(object sender, EventArgs e)
        {
            TextBox txtChildNum = (TextBox)sender;
            int txtChildnum;
            if (txtChildNum.Text.Trim() == "" || txtChildNum.Text == null)
            {
                txtChildnum = 0;
            }
            else
            {
                txtChildnum = int.Parse(txtChildNum.Text);

            }

            // Get the container panel
            //Panel pnlRoom = (Panel)txtChildNum.NamingContainer;
            ListViewDataItem pnlRoom = (ListViewDataItem)(sender as Control).NamingContainer;

            // find number of adults
            // TextBox Child_1 = (TextBox)pnlRoom.FindControl("childNum0");

            // add number of kids
            for (int j = 0; j < txtChildnum; j++)
            {
                string chldID = "childAge" + (j + 1);
                TextBox Child_txt = (TextBox)pnlRoom.FindControl(chldID);
                Child_txt.Enabled = true;

                if (Page.IsPostBack && "childAge1" != string.Empty)
                {
                    Child_txt.Visible = false;

                }
                if (Page.IsPostBack && "childAge2" != string.Empty)
                {
                    Child_txt.Visible = false;

                }
                if (Page.IsPostBack && "childAge3" != string.Empty)
                {
                    Child_txt.Visible = false;

                }

            }
        }
    }
}