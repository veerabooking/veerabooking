﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Veerabook
{
    public partial class WebForm5 : System.Web.UI.Page
    {
        protected string roomStr ;
        protected string roomsStr ;
        protected string txtChldAge ;
        protected string txtChldStr ;
        protected string txtAdltStr ;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                string today = DateTime.Now.ToString("yyyy-MM-dd");
                DateTime endDate = Convert.ToDateTime(today);
                Int64 addedDays = Convert.ToInt64(2);
                endDate = endDate.AddDays(addedDays);
                string end = endDate.ToString("yyyy-MM-dd");
                //startDate.Text = "From " + today + " To " + end;
                //chckIn.Text = today;
                //chckOut.Text = end;
            }
        }

        protected void btnSearchHotel_Click(object sender, EventArgs e)
        {
            login();
            searchData();

            // xresult.Text = Session["htlSrchPostData"].ToString() ;
            Response.Redirect("HotelListing.aspx");
        }

        private void searchData()
        {
            //ListView newLvRooms = (ListView)lvRooms;
            int x = 0;
            //foreach (ListViewItem item in newLvRooms.Items)
            //{

            //    //ListViewItem pnlRoom = newLvRooms.FindControl("pnlRoom") as ListViewItem;
            //    ListViewItem pnlRoom = newLvRooms.Items[x] as ListViewItem;

            //    TextBox txtAdultNum = (TextBox)pnlRoom.FindControl("adltNum");
            //    txtAdltStr = "<RoomType>" + txtAdultNum.Text + "</RoomType>";
            //    TextBox txtChildNum = (TextBox)pnlRoom.FindControl("childNum");
            //    txtChldAge = null;
            //    if (int.Parse(txtChildNum.Text) > 0)
            //    {
            //        for (int i = 0; i < int.Parse(txtChildNum.Text); i++)
            //        {
            //            string chldAgeID = "childAge" + (i + 1);
            //            TextBox txtChildAge = (TextBox)pnlRoom.FindControl(chldAgeID);
            //            txtChldAge += "<Age>" + txtChildAge.Text + "</Age>";
            //        }
            //        txtChldStr = "<Children>" + txtChldAge + "</Children>";
            //    }
            //    roomStr += "<Room>" + txtAdltStr + txtChldStr + "</Room>";
            //    x++;
            //}
            roomsStr = "<Rooms>" + roomStr + "</Rooms>";

            //string postData = "xml=" + "<MultireisenAPI_Request><RequestHeader><LoginName>admin@dynawix.com</LoginName><SessionId>" + Session["SessionId"].ToString() + "</SessionId><Language>en</Language><AsyncMode>FALSE</AsyncMode></RequestHeader><SearchData><Hotel><Destination><CityCode></CityCode><CityName>" + txtDestinations.Text + "</CityName></Destination><CheckIn>" + chckIn.Text + "</CheckIn><CheckOut>" + chckOut.Text + "</CheckOut>" + roomsStr + "<Category></Category></Hotel></SearchData></MultireisenAPI_Request>";
            //Session["htlSrchPostData"] = postData;
            //Session["checkIn"] = chckIn.Text;
            //Session["checkOut"] = chckOut.Text;
        }

        protected void ddlNoRooms_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("dt");
            dt.Columns.Add("ID");
            //for (int i = 0; i < int.Parse(ddlNoRooms.SelectedValue.ToString()); i++)
            //{
            //    // TRY LISTVIEW TO LOOP ON ROOM DATA
            //    DataRow dr = dt.NewRow();
            //    dr[0] = i.ToString();


            //    dt.Rows.Add(dr);
                
            //}

            ds.Tables.Add(dt);
            //lvRooms.DataSource = ds;
            //lvRooms.DataBind();
        }

        //private void ChildNum_TextChanged(object sender, EventArgs e)
        //{
        //    TextBox txtChildNum = (TextBox)sender;
        //    // Get the container panel
        //    Panel pnlRoom = (Panel)txtChildNum.NamingContainer;

        //    // find number of adults
        //    // TextBox Child_1 = (TextBox)pnlRoom.FindControl("childNum0");

        //    // add number of kids
        //    for (int j = 0; j < int.Parse(txtChildNum.Text); j++)
        //    {
        //        TextBox childAge = new TextBox();
        //        childAge.ID = "childAge" + j;
        //        childAge.CssClass = "input-group form-control marginRight";
        //        childAge.Attributes.Add("type", "number");
        //        childAge.Attributes.Add("name", "childAge");
        //        string plchlrChildAge = "age child" + (j + 1);
        //        childAge.Attributes.Add("placeholder", plchlrChildAge);

        //        // add details to panel room container
        //        pnlRoom.Controls.Add(childAge);
        //    }
        //}
        
        protected void login()
        {
            WebRequest request = WebRequest.Create("http://api-test.multireisen.com/login/");
            request.Method = "POST";
            string postData = "xml=" + "<MultireisenAPI_Request><Login><LoginName>admin@dynawix.com</LoginName><Passw>123</Passw></Login></MultireisenAPI_Request>";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse response = request.GetResponse();

            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            //xresult.Text = responseFromServer;
            //StringReader theReader = new StringReader(xresult.Text);
            DataSet OriginalDataSet = new DataSet("dataSet");

            //OriginalDataSet.ReadXml(theReader);

            Session["SessionId"] = OriginalDataSet.Tables["Login"].Rows[0]["SessionID"];




            reader.Close();
            dataStream.Close();
            response.Close();

        }
        protected void fillRoomStr()
        {

        }

        protected void startDate_TextChanged(object sender, EventArgs e)
        {

        }
        protected void childNum_TextChanged(object sender, EventArgs e)
        {
            TextBox txtChildNum = (TextBox)sender;
            int txtChildnum;
            if (txtChildNum.Text.Trim() == "" || txtChildNum.Text == null)
            {
                txtChildnum = 0;
            }
            else
            {
                txtChildnum = int.Parse(txtChildNum.Text);

            }

            // Get the container panel
            //Panel pnlRoom = (Panel)txtChildNum.NamingContainer;
            ListViewDataItem pnlRoom = (ListViewDataItem)(sender as Control).NamingContainer;

            // find number of adults
            // TextBox Child_1 = (TextBox)pnlRoom.FindControl("childNum0");

            // add number of kids
            for (int j = 0; j < txtChildnum; j++)
            {
                string chldID = "childAge" + (j + 1);
                TextBox Child_txt = (TextBox)pnlRoom.FindControl(chldID);
                Child_txt.Enabled = true;
            }

        }
    }
}