﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="HotelDetailPages.aspx.cs" Inherits="Veerabook.HotelDetailPages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    <asp:Literal Text="Hotel Name" ID="lblHotelName" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHeader" runat="server">

    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/media.css">
    <link rel="stylesheet" href="assets/js/wizard/css/jquery.steps.css">
    <link type="text/css" rel="stylesheet" href="assets/js/wizard/jquery.stepy.css" />
    <link href="assets/js/tabs/acc-wizard.min.css" rel="stylesheet">
    <link href="css/listviewStyles.css" rel="stylesheet" />
    <style>
        body .tlbr {
            padding-bottom: 80px !important;
        }
        body .actions {
            display:none !important;
        }
        .carousel-inner  img {
            height: 500px !important
        }
    </style>
    
  
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    
    <div class="container-fluid">
    <div class="video-text">
        <h2><asp:Label runat="server" ID="Hotel_name"></asp:Label></h2>
        <p><asp:Literal runat="server" ID="Hotel_Address"></asp:Literal></p>
        <div class="star-icon">
            <!-- ngIf: hoteldetail.HotelRating == 'Special Category' -->
            <!-- ngRepeat: n in range(hoteldetail.HotelRating) -->
            <%--<i class="fa fa-star star-icon-padding ng-scope" aria-hidden="true"></i>--%>
            <!-- end ngRepeat: n in range(hoteldetail.HotelRating) -->
            <%--<i class="fa fa-star star-icon-padding ng-scope" aria-hidden="true"></i>--%>
                 
               <asp:Literal runat="server" id="Hotel_Category"> </asp:Literal> 
                

            <!-- end ngRepeat: n in range(hoteldetail.HotelRating) -->
        </div>
    </div>
        </div>
    <div class="col-md-8">
            
        
            <div class="vendor">
                <div data-ride="carousel" class="carousel slide" id="carousel-dua">
                    <ol class="carousel-indicators">
                        <li class="active" data-slide-to="0" data-target="#carousel-dua"></li>
                        <%--<li data-slide-to="1" data-target="#carousel-dua" class=""></li>
                        <li data-slide-to="2" data-target="#carousel-dua" class=""></li>--%>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <img class="img-responsive-media" id="first_slide" alt="First slide" data-src="holder.js/900x500/auto/#777:#555/text:First slide" runat="server" />
                        </div>
                       <asp:ListView runat="server" id="HotelImgslideshow">
                           <ItemTemplate>
                               <div class="item">
                                   <img class="img-responsive-media" alt="Second slide" data-src="holder.js/900x500/auto/#666:#444/text:Second slide" src="<%#Eval("ContentUrl") %>">
                               </div>
                           </ItemTemplate>
                       </asp:ListView>
                        
                     
                       
                    </div>
                    <a data-slide="prev" href="#carousel-dua" class="left carousel-control">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a data-slide="next" href="#carousel-dua" class="right carousel-control">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>

            </div>
        </div>
            <div class="col-md-4">
                <div style="width:100%;   height:300px;   frameborder:0px;   scrolling: no;   marginheight:90px;   marginwidth:0px;" id="mapDiv"></div>
<%--                <iframe  width="100%"   height="300"   frameborder="0"   scrolling="no"   marginheight="90"   marginwidth="0"   src="https://maps.google.com/maps?q=<%=Session["Hotel_Location_LAT"] %>,<%=Session["Hotel_Location_LNG"] %>&hl=es;z=14&amp;output=embed"> </iframe>--%>
                <br />
                <small><a  href="https://maps.google.com/maps?q='+data.lat+','+data.lon+'&hl=es;z=14&amp;output=embed" style="color:#0000FF;text-align:left"  target="_blank" > See map bigger</a></small>
            </div>
   
    <%-- List View --%>
    
    
    <div class="col-xs-12">
        <div class="row col-xs-12 noPaddingMediaXS">
            <div class="col-xs-12 marginTop roomsHeader">
                <div class="col-xs-3 text-center headerItem">Room Types</div>
                <div class="col-xs-1 text-center headerItem">Pax</div>
                <div class="col-xs-3 text-center headerItem">Inclusions</div>
                <div class="col-xs-3 text-center headerItem">Price</div>
                <div class="col-xs-2 text-center headerItem"></div>
            </div>
        </div>
        <div class="row col-xs-12 lvRoomsCont">
            <div class="row paddingSide">
                <asp:ListView ID="lvRooms" runat="server" OnItemDataBound="lvRooms_ItemDataBound">
                <ItemTemplate>
                <div class="col-xs-12 lvRoomsRow">
                    <div class="col-xs-3 text-center dataItem"><%#Eval("Room_Name") %></div>
                    
                    <div class="col-xs-1 text-center dataItem roomCapacity">
                        <i class="fa fa-user"></i>
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="col-xs-3 text-center dataItem"><%# Eval("Meal_Name") %></div>
                    <div class="col-xs-3 text-center dataItem">USD <%# Eval("Room_TotalPrice")%></div>
                    <div class="col-xs-2 text-center buttonItem">
<%--                        <asp:Button CssClass="col-xs-12 btn btn-primary" Text="Book" runat="server"/>--%>
                        <asp:LinkButton id="btnsubmitRoom" Text="book" CssClass="col-xs-12 btn btn-primary" PostBackUrl='<%# string.Format("~/Booking.aspx?RoomId={0}&HotelId={1}", Eval("Room_ID"), Eval("Hotel_ID")) %>' runat="server" />
                    </div>
                </div>
                <div class="alert alert-danger">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <span class="entypo-attention"></span>
                                    <strong>Oh snap!</strong>&nbsp;&nbsp;
                    Getremarks.aspx?hotelid=<%#Eval("hotel_id") %>&roomid=<%#Eval("room_id") %>&sessionid=<%#Eval("sessionid") %>&searchtoken=<%#Eval("searchtoken") %>
              </div>


                </ItemTemplate>
            </asp:ListView>
            </div>
        </div>  
    </div>


    <%-- End List View  --%>
    
    
    

    <%-- About hotel --%>

    <div class="col-md-12">
        <div class="nest" id="basicClose">
            <div class="title-alt">
                <h6>
                    Hotel Amenities</h6>
                <div class="titleClose">
                </div>
                <div class="titleToggle">
                    <a class="nav-toggle-alt" href="#basic">
                        <span class="entypo-up-open"></span>
                    </a>
                </div>

            </div>

            <div class="body-nest" id="basic">

                <div id="wizard">
                    <h2>About The Hotel</h2>
                    <section>
                        <h4>Location : </h4>
<%--                        <p>This hotel is located in the heart of Dubai city, just 10 minutes away from
    Dubai International Airport and within walking
    distance of famous shopping malls, business centres and wholesale markets.</p>--%>
                        <p><asp:Label Text="trial location text" ID="lblLocation" runat="server" /></p>
                        <br/>
                        <h4>Description :  </h4>
                        <p><asp:Label Text="trial Description text" ID="lblDescription" runat="server" /></p>
                    </section>

                    <h2>Hotel Amenities</h2>
                    <section style="column-count:3;">
                        <asp:ListView ID="lvAmenities" runat="server">
                            <ItemTemplate>
<%--                                <P><asp:label Text="<%#Eval("Hotel_Amenities") %>" ID="lblamin" runat="server"></asp:label> </P>--%>
<%--                                <p><%#Eval("Hotel_Amenities") %></p>--%>
                            </ItemTemplate>
                        </asp:ListView>
             
                    </section>

                    <h2>Nearest Landmarks</h2>
                    <section>

                       

                        <div style="width:100%;   height:450px;   frameborder:0px;   scrolling: no;   marginheight:90px;   marginwidth:0px;" id="map"></div>
<%--                        <iframe  width="600"   height="450"   frameborder="0"   scrolling="no"   marginheight="90"   marginwidth="0"   src="https://maps.google.com/maps?q=<%=Session["Hotel_Location_LAT"] %>,<%=Session["Hotel_Location_LNG"] %>&hl=es;z=14&amp;output=embed"> </iframe>--%>
                        <br />
                        <small><a  href="https://maps.google.com/maps?q='+data.lat+','+data.lon+'&hl=es;z=14&amp;output=embed" style="color:#0000FF;text-align:left"  target="_blank" > See map bigger</a></small>
                    </section>

                </div>

            </div>
        </div>
    </div>

    
    <%-- END  About hotel --%>
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">    

<!-- MAIN EFFECT -->

    <script type="text/javascript" src="assets/js/main.js"></script>
    <script src="assets/js/wizard/lib/jquery.cookie-1.3.1.js"></script>
    <script src="assets/js/wizard/build/jquery.steps.js"></script>
    <script type="text/javascript" src="assets/js/wizard/jquery.stepy.js"></script>
    
    
    
    
    
    <script>
        $(function() {
            $("#wizard").steps({
                headerTag: "h2",
                bodyTag: "section",
                transitionEffect: "slideLeft"
            });

            $("#wizard_vertical").steps({
                headerTag: "h2",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                stepsOrientation: "vertical"
            });

            $("#wizard-tab").steps({
                headerTag: "h2",
                bodyTag: "section",
                transitionEffect: "none",
                enableFinishButton: true,
                enablePagination: true,
                enableAllSteps: true,
                titleTemplate: "#title#",
                cssClass: "tabcontrol"
            });

            //stepy
            $('#transition-duration-demo').stepy({
                duration: 400,
                transition: 'fade'
            });

        });

    </script>
    <script>
        // Initialize and add the map
        function initMap() {
            
            var hotelName = $("#CPContent_Hotel_name").text();
            //alert(hotelName);
          // The location of Uluru
          var uluru = {lat: <%=Session["Hotel_Location_LAT"] %>, lng: <%=Session["Hotel_Location_LNG"] %>};
          // The map, centered at Uluru
          var map = new google.maps.Map(
              document.getElementById('map'), {zoom: 13, center: uluru});
            // The marker, positioned at Uluru
          var marker = new google.maps.Marker({ position: uluru, map: map });
          var infoWindow = new google.maps.InfoWindow({
              content: '<h5>'+hotelName+'</h5>'
          });
          marker.addListener('click', function() {
              infoWindow.open(map,marker);
          });
            
          var map2 = new google.maps.Map(
              document.getElementById('mapDiv'), {zoom: 13, center: uluru});
          var marker2 = new google.maps.Marker({ position: uluru, map: map2 });
          var infoWindow2 = new google.maps.InfoWindow({
              content: '<h5>'+hotelName+'</h5>'
          });
          marker2.addListener('click', function() {
              infoWindow2.open(map2,marker2);
          });
          
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3Fdde_DRilu_UaQWnxnsPf3xkioxOBNg&callback=initMap" ></script>

    <%--<script>
        $(document).ready(function() {
            $(".disabled" ).css("class", "enabled done");
           //$("li:class(disabled)").addClass("enabled done");
           //$("li:class(disabled)").removeClass("disabled");
           //return;

        });
    </script>--%>
</asp:Content>
