﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" Debug="true" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="databaseWithDDL.aspx.cs" Inherits="Veerabook.databaseWithDDL" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    Get Database Data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHeader" runat="server">

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <style>
        body .tableFields > * {
        
        line-height:  25px;
        margin-right: 5px !important;

        }
        body .tableFields .form-control {
            width : 15% !important;
        }
        /* modal styles*/
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }
        #CPContent_Panel1 input[type=text] {
            color:black !important;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 100%;
            height: auto;
        }
        /* modal styles*/
        .btn#btnEditCol {
            font-size: 8px !important;
            margin-right: 5px;
        }
        .btn#btnDropCol {
            font-size: 8px !important;
        }
        .tableFields input[type=text] , .modalPopup input[type=text] {
            color:unset !important;
        }

    </style>
    <style>
        ul.ui-menu.ui-widget.ui-widget-content.ui-autocomplete.ui-front {
            list-style-type: none !important;
            background-color: white;
            width: 13% !important;
        }
    </style>
    <script>
        $(function () {
            var availableTags = [
              "char(n)",
              "varchar(n)",
              "varchar(max)",
              "text",
              "nchar",
              "nvarchar",
              "nvarchar(max)",
              "ntext",
              "binary",
              "varbinary",
              "varbinary(max)",
              "image",
            ];
            $(".txtDataType").autocomplete({
                source: availableTags
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">

    <!-- ModalPopupExtender -->
    <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="btnAddCol"
        CancelControlID="btnClose" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
    <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup col-sm-10" align="center" Style="display: none">
        <div class="col-xs-12 marginTop">
            <asp:TextBox ID="txtColName" placeholder="Column name" CssClass="col-xs-6" runat="server" />
            <asp:TextBox ID="txtColDataType" placeholder="Data type" CssClass="col-xs-6" runat="server" />

            <br />
            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-success marginTopSm" OnClick="btnSubmit_Click" Text="Submit" />
            <asp:Button ID="btnClose" runat="server" CssClass="btn btn-secondary marginTopSm" Text="Close" />
        </div>
    </asp:Panel>
    <!-- ModalPopupExtender -->
    <!-- ModalPopupExtender -->
    <cc1:ModalPopupExtender ID="mp2" runat="server" PopupControlID="Panel2" TargetControlID="btnAddTbl"
        CancelControlID="btnClose2" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
    <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup col-sm-10" align="center" Style="display: none">
        <div class="col-xs-12 marginTop">
            <asp:UpdatePanel ID="upColsTbl" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <asp:TextBox ID="txtTableName" placeholder="Table name" CssClass="col-xs-4 marginRight" runat="server" />
                        <asp:TextBox ID="txtCountCols" AutoPostBack="true" placeholder="Number Of Columns" ClientIDMode="Static" type="number" OnTextChanged="txtCountCols_TextChanged" CssClass="col-xs-4" runat="server" />
                    </div>
                    <asp:ListView ID="lvColData" ClientIDMode="Static" runat="server">
                        <ItemTemplate>
                            <div class="row marginTopSm">
                                <asp:TextBox ID="txtColName2" placeholder="Column name" CssClass="col-sm-2 marginRight" runat="server" />
                                <asp:TextBox ID="txtDataType" placeholder="Data type" CssClass="col-sm-2 marginRight" runat="server" />
                                <asp:DropDownList ID="ddlIdentity" CssClass="btn btn-default col-sm-2 marginRight" runat="server">
                                    <asp:ListItem Text="Is Identity" />
                                    <asp:ListItem Text="YES" />
                                    <asp:ListItem Text="NO" />
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlPrimary" CssClass="btn btn-default col-sm-2 marginRight" runat="server">
                                    <asp:ListItem Text="Is Primary" />
                                    <asp:ListItem Text="YES" />
                                    <asp:ListItem Text="NO" />
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlNull" CssClass="btn btn-default col-sm-2 marginRight" runat="server">
                                    <asp:ListItem Text="Is Null" />
                                    <asp:ListItem Text="YES" />
                                    <asp:ListItem Text="NO" />
                                </asp:DropDownList>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <asp:Button ID="btnSubmitTbl" runat="server" CssClass="btn btn-success marginTopSm" OnClick="btnSubmitTbl_Click" Text="Submit" />
            <asp:Button ID="btnClose2" runat="server" CssClass="btn btn-secondary marginTopSm" Text="Close" />
        </div>
        <br />
        <div class="clearfix"></div>
    </asp:Panel>
    <!-- ModalPopupExtender -->
    <div class="col-xs-12 marginTop">
        <asp:DropDownList ID="ddlDatabaseTables" OnSelectedIndexChanged="ddlDatabaseTables_SelectedIndexChanged" CssClass="btn btn-default" AutoPostBack="true" runat="server">
            <%--<asp:ListItem Text="select" Value="-1" />--%>
        </asp:DropDownList>
        <asp:Button Text="Add a new Table" ID="btnAddTbl" CssClass="btn btn-success" runat="server" />
        <asp:Button Text="Delete Table" ID="btnDropTable" CssClass="btn btn-danger" OnClick="btnDropTable_Click" runat="server" />
    </div>
    <%--    <asp:Button Text="load" ID="btnLoadDDL" OnClick="btnLoadDDL_Click" CssClass="btn btn-success" runat="server" />--%>
    <div class="col-xs-12 marginTop">
        <asp:GridView runat="server" CssClass="col-xs-12" ID="gvTrial"></asp:GridView>
    </div>
    <div class="col-xs-12 marginTop">
        <asp:Button Text="add Column to Table" ID="btnAddCol" CssClass="btn btn-success" runat="server" />
    </div>
    <div class="col-xs-12 marginTop">
        <asp:ListView Visible="false" OnItemDataBound="lvTableFields_ItemDataBound" ClientIDMode="Static" ID="lvTableFields" runat="server">
            <ItemTemplate>
                <div class="row marginTopSm tableFields">
                    <asp:Label ID="lblFieldName" Text='<%#Eval("COLUMN_NAME") %>' CssClass="col-xs-1" runat="server"></asp:Label>
                    <asp:DropDownList OnSelectedIndexChanged="ddlFieldType_SelectedIndexChanged" CssClass="col-xs-2 filter-status form-control" ID="ddlFieldType" runat="server">
                        <asp:ListItem Text="text 1"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList CssClass="col-xs-2 filter-status form-control" runat="server">
                        <asp:ListItem Text="text1" />
                        <asp:ListItem Text="text2" />
                    </asp:DropDownList>
                    <asp:TextBox CssClass="col-xs-2" runat="server" />
                    <asp:TextBox placeholder="new datatype" ID="txtDataType" ClientIDMode="Static" CssClass="col-xs-2 txtDataType" runat="server" />

                    <div class="col-xs-1">
                        <button id="btnEditCol" onserverclick="btnEditCol_Click" class="btn btn-default col-xs-5" runat="server"><i class="fa fa-edit"></i></button>
                        <button id="btnDropCol" onserverclick="btnDropCol_Click" class="btn btn-danger col-xs-5" runat="server"><span><i class="glyphicon glyphicon-trash"></i></span></button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </ItemTemplate>
        </asp:ListView>
    </div>

    <%--    <div class="row">

      <div class="ui-widget">
  <label for="tags">Tags: </label>
  <input id="tags">
</div>

    </div>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
