﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Web.Security;


namespace Veerabook
{
    public partial class HotelListing : System.Web.UI.Page
    {
        Xtools xtools = new Xtools() ;
        Multi multi = new Multi();
        Merger merger = new Merger();
        public string filterString = "";
        public string StarFilterStr = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindHotelList();
            }
        }


      

        void BindHotelList()
        {


            //Get Session Search Token Based On what's Sent 
            Session["SessionSearchToken"] = multi.Search(Session["SessionId"].ToString(), Session["htlSrchPostData"].ToString());


            //Call the Binding Line 
            BindHotelLv();
         
          
        }

        void BindHotelLv()
        {
           
            lvHotelsList.DataSource = xtools.RemoveDuplicateRows(merger.FinalSearchData(Session["SessionSearchToken"].ToString()),"Hotel_Name");
            lvHotelsList.DataBind();
          
        }










   



        protected void lvHotelsList_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            //set current page startindex, max rows and rebind to false
            lvDataPager1.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);

            //rebind List View
            string text = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "Cache/" + Session["SessionSearchToken"] + ".XML");

            StringReader theReader = new StringReader(text);
            DataSet originalDataSet = new DataSet("dataSet");
            originalDataSet.ReadXml(theReader);
            lvHotelsList.DataSource = xtools.RemoveDuplicateRows(GetRowsByFilter(originalDataSet, filterString).Tables[0], "Hotel_Name");
            lvHotelsList.DataBind();
        }

        protected void lvHotelsList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                //Get Hotel Image from the Api
                Image htlImg = (Image)e.Item.FindControl("htlImg");
                System.Data.DataRowView rowView = e.Item.DataItem as System.Data.DataRowView;
                htlImg.ImageUrl = xtools.getHotelImages(Convert.ToString(rowView["Hotel_Name"]) + " " + Convert.ToString(rowView["Hotel_City_Name"])  ,1).Rows[0]["contentUrl"].ToString() ;

                //Register the Repeater and Bind it 
                Repeater roomsRpt = (Repeater) e.Item.FindControl("RoomsRpt");
                //GridView gv1 = (GridView)e.Item.FindControl("gv1");

                // Register the Literal Control of Stars 
                Literal Hotelstars = (Literal)e.Item.FindControl("Hotelstars");
                //Get Room Price
                string text = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "Cache/" + Session["SessionSearchToken"] + ".XML");
                StringReader theReader = new StringReader(text);
                DataSet originalDataSet = new DataSet("dataSet");
                originalDataSet.ReadXml(theReader);
                //Fix for Stars Issue 
               // int hotelCatVal = int.Parse(rowView["Hotel_Category"].ToString());
               // string stars = "";
               // for (var i = 0; i < hotelCatVal; i++)
               // {
               //     stars += "<i class='fa fa-star'></i> ";
               // }

                Hotelstars.Text = "3";
                DataView view = new DataView(originalDataSet.Tables[0]);
                view.RowFilter = "Hotel_Name = '" + rowView["Hotel_Name"].ToString() + "'";
                view.Sort = "Room_TotalPrice ASC";

                //view.Tables[0].Columns.Add("column_1", typeof(string));
                roomsRpt.DataSource =  new FilterRows(view, 2);
                roomsRpt.DataBind();





            }



        }










        private DataSet GetRowsByFilter(DataSet myDataSet, string expressionFilter)
        {
            DataTable table = myDataSet.Tables[0];
            // Presuming the DataTable has a column named Date.
            string expression;
            expression = expressionFilter;
            DataRow[] foundRows;
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            dt.Columns.Add("Hotel_ID");
            dt.Columns.Add("Hotel_Name");
            dt.Columns.Add("Hotel_Category");
            dt.Columns.Add("Hotel_Address");
            dt.Columns.Add("Hotel_Phone");
            dt.Columns.Add("Hotel_City_Name");
            dt.Columns.Add("Hotel_City_Code");
            dt.Columns.Add("Hotel_Location_LAT");
            dt.Columns.Add("Hotel_Location_LNG");
            dt.Columns.Add("Room_ID");
            dt.Columns.Add("Room_Name");
            dt.Columns.Add("Meal_Code");
            dt.Columns.Add("Meal_Name");
            dt.Columns.Add("Cancellation_Deadline");
            dt.Columns.Add("Cancellation_Policy");
            dt.Columns.Add("Room_TotalPrice");
            dt.Columns.Add("Room_BasePrice");
            dt.Columns.Add("Room_BasePrice_Currency");
            dt.Columns.Add("Supplier");
            dt.Columns.Add("SessionId");
            dt.Columns.Add("SearchToken");

            // Use the Select method to find all rows matching the filter.
            foundRows = table.Select(expression);

            // Print column 0 of each returned row.
            for (int i = 0; i < foundRows.Length; i++)
            {

                DataRow newRow = dt.NewRow();
                newRow["Hotel_ID"] = foundRows[i]["Hotel_ID"];
                newRow["Hotel_Name"] = foundRows[i]["Hotel_Name"];
                newRow["Hotel_Category"] = foundRows[i]["Hotel_Category"];
                newRow["Hotel_Address"] = foundRows[i]["Hotel_Address"];
                newRow["Hotel_Phone"] = foundRows[i]["Hotel_Phone"];
                newRow["Hotel_City_Name"] = foundRows[i]["Hotel_City_Name"];
                newRow["Hotel_City_Code"] = foundRows[i]["Hotel_City_Code"];
                newRow["Hotel_Location_LAT"] = foundRows[i]["Hotel_Location_LAT"];
                newRow["Hotel_Location_LNG"] = foundRows[i]["Hotel_Location_LNG"];
                newRow["Room_ID"] = foundRows[i]["Room_ID"];
                newRow["Room_Name"] = foundRows[i]["Room_Name"];
                newRow["Meal_Code"] = foundRows[i]["Meal_Code"];
                newRow["Meal_Name"] = foundRows[i]["Meal_Name"];
                newRow["Cancellation_Deadline"] = foundRows[i]["Cancellation_Deadline"];
                newRow["Cancellation_Policy"] = foundRows[i]["Cancellation_Policy"];
                newRow["Room_BasePrice"] = foundRows[i]["Room_BasePrice"];
                newRow["Room_TotalPrice"] = foundRows[i]["Room_TotalPrice"];
                newRow["Room_BasePrice_Currency"] = foundRows[i]["Room_BasePrice_Currency"];
                newRow["Supplier"] = "Multi";

                newRow["SessionId"] = foundRows[i]["SessionId"];
                newRow["SearchToken"] = foundRows[i]["SearchToken"];

                dt.Rows.Add(newRow);
            }
            dt.TableName = "Search_Result";
            ds.Tables.Add(dt);
            return ds;
        }

        protected int getMinPrice()
        {
            string priceRangeStr;
            int Min = 0;
            priceRangeStr = amount.Value;

            String searchString = "$";
            int startIndex = priceRangeStr.IndexOf(searchString) + 1;
            searchString = " -";
            int endIndex = priceRangeStr.IndexOf(searchString);
            String substring = priceRangeStr.Substring(startIndex, endIndex - startIndex);
            Min = int.Parse(substring.Trim());
            return Min;

        }
        protected int getMaxPrice()
        {
            string priceRangeStr;
            int Max = 5000;
            priceRangeStr = amount.Value;

            String searchString = "- $";
            int startIndex = priceRangeStr.IndexOf(searchString) +3;
            searchString = "!";
            int endIndex = priceRangeStr.IndexOf(searchString);
            String substring = priceRangeStr.Substring(startIndex, endIndex - startIndex);
            Max = int.Parse(substring.Trim());
            return Max;

        }

        protected void btnApplyFilters_Click(object sender, EventArgs e)
        {

            //for hotel name/chain filter
            if (lblhotelname.Value.Trim() != "" && lblhotelname.Value.Trim() != null)
            {
                if (filterString != "" && filterString != null)
                {
                    filterString += " AND ";
                }
                filterString += "Hotel_Name LIKE '%" + lblhotelname.Value.ToString() + "%'";
            }

            //for location filer
            if (lblloc.Value.Trim() != "" && lblloc.Value.Trim() != null)
            {
                if (filterString != "" && filterString != null)
                {
                    filterString += " AND ";
                }
                filterString += "Hotel_Address LIKE '%" + lblloc.Value.ToString() + "%'";
            }

            //for star rating filter

            if (Star1.Checked)
            {
                if (StarFilterStr.Trim() != "" && StarFilterStr != null)
                {
                    StarFilterStr += " OR ";
                }
                StarFilterStr += "(Hotel_Category = " + int.Parse(Star1.Text) + ")";

            }
            if (Star2.Checked)
            {
                if (StarFilterStr.Trim() != "" && StarFilterStr != null)
                {
                    StarFilterStr += " OR ";
                }
                StarFilterStr += "(Hotel_Category = " + int.Parse(Star2.Text) + ")";

            }
            if (Star3.Checked)
            {
                if (StarFilterStr.Trim() != "" && StarFilterStr != null)
                {
                    StarFilterStr += " OR ";
                }
                StarFilterStr += "(Hotel_Category = " + int.Parse(Star3.Text) + ")";

            }
            if (Star4.Checked)
            {
                if (StarFilterStr.Trim() != "" && StarFilterStr != null)
                {
                    StarFilterStr += " OR ";
                }
                StarFilterStr += "(Hotel_Category = " + int.Parse(Star4.Text) + ")";

            }
            if (Star5.Checked)
            {
                if (StarFilterStr.Trim() != "" && StarFilterStr != null)
                {
                    StarFilterStr += " OR ";
                }
                StarFilterStr += "(Hotel_Category = " + int.Parse(Star5.Text) + ")";

            }
            if (StarFilterStr.Trim() != "" && StarFilterStr != null)
            {
                if (filterString.Trim() != "" && filterString != null)
                {
                    filterString += " AND ";
                }
                filterString += " ( " + StarFilterStr + " ) ";
            }
            //if (int.Parse(ddlStarRatingFilter.SelectedValue) > 1)
            //{
            //    if (filterString.Trim() != "" && filterString != null)
            //    {
            //        filterString += " AND ";
            //    }
            //    filterString += "Hotel_Category >= " + int.Parse(ddlStarRatingFilter.SelectedValue) ;
            //}

            // for Price Range filter
            int MinPrice = 0;
            int MaxPrice = 5000;
            if (amount.Value != "$0 - $5000")
            {
                 MinPrice = getMinPrice();
                 MaxPrice = getMaxPrice();
            }
            
            //if (MinPrice > 1 || MaxPrice < 5000)
            //{
            //    if (filterString.Trim() != "" && filterString != null)
            //    {
            //        filterString += " AND ";
            //    }
            //    filterString += "Room_TotalPrice >= CAST(" + MinPrice + " AS int)";
            //}

            string text = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "Cache/" + Session["SessionSearchToken"] + ".XML");

            StringReader theReader = new StringReader(text);
            DataSet originalDataSet = new DataSet("dataSet");
            originalDataSet.ReadXml(theReader);
            lvHotelsList.DataSource = xtools.RemoveDuplicateRows(GetRowsByFilter(originalDataSet, filterString).Tables[0], "Hotel_Name"); ;
            lvHotelsList.DataBind();
        }
    }
    public class FilterRows : IEnumerable
    {
        DataView dataView;
        private int rowsToShow;

        public FilterRows(DataView dataView, int rowsToShow)
        {
            this.rowsToShow = rowsToShow;
            this.dataView = dataView;
        }

        public IEnumerator GetEnumerator()
        {
            return new PageOfData(this.dataView.GetEnumerator(), this.rowsToShow);
        }


        internal class PageOfData : IEnumerator
        {
            private IEnumerator e;
            private int cnt = 0;
            private int rowsToShow;

            internal PageOfData(IEnumerator e, int rowsToShow)
            {
                this.rowsToShow = rowsToShow;
                this.e = e;
            }

            public object Current
            {
                get { return e.Current; }
            }

            public bool MoveNext()
            {
                // If we've hit out limit return false
                if (cnt >= rowsToShow)
                    return false;

                // Track the current row
                cnt++;

                return e.MoveNext();
            }

            public void Reset()
            {
                e.Reset();
                cnt = 0;
            }
        }
    }




}
