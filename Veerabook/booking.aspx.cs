﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Veerabook
{
    public partial class booking : System.Web.UI.Page
    {
        private Xtools xtools = new Xtools();
        private object dv;
        Multi multi = new Multi();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                roomSelect();
                //loadhoteldetails();
                gvRoomData.DataSource = getRoomData();
                gvRoomData.DataBind();
                totalNights();
                //BindHotelItemLv();
                createTrvlData();
                
            }
        }
        protected void loadhoteldetails()
        {
            //HotelName.Text = Request.QueryString["HotelId"].ToString();

        }
        void totalNights()
        {

            DateTime checkInDate = DateTime.Parse(Session["checkIn"].ToString());
            DateTime checkOutDate = DateTime.Parse(Session["checkOut"].ToString());
            double totalNight = (checkOutDate - checkInDate).TotalDays;
            TotalNights.Text = totalNight.ToString();
            CheckIn.Text = checkInDate.ToString();
            CheckOut.Text = checkOutDate.ToString();

        }
        protected DataTable getRoomData()
        {


            string text = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "Cache/" + Session["SessionSearchToken"] + ".XML");

            StringReader theReader = new StringReader(text);
            DataSet originalDataSet = new DataSet("dataSet");
            originalDataSet.ReadXml(theReader);

            //Get Only the Data of This Hotel out of the Cached XML 
            DataView view = new DataView(originalDataSet.Tables[0]);
            view.RowFilter = "Hotel_ID = '" + Session["Hotel_ID"].ToString() + "'";
            var dv = view.ToTable();
            int choosenRoomId = int.Parse(Request.QueryString["RoomId"].ToString());
            TotalPrice.Text = dv.Rows[choosenRoomId]["Room_TotalPrice"].ToString();
            HotelTextName.Text = dv.Rows[0]["Hotel_Name"].ToString();
            if (dv.Rows[0]["Hotel_Address"].ToString() == null || dv.Rows[0]["Hotel_Address"].ToString().Trim() == "")
            {
                HotelAddress.Text = "Address is not available";
            }
            else
            {
                HotelAddress.Text = dv.Rows[0]["Hotel_Address"].ToString();
            }
            if (dv.Rows[0]["Hotel_Phone"].ToString() == null || dv.Rows[0]["Hotel_Phone"].ToString().Trim() == "")
            {
                HotelPhone.Text = "Phone number is not available";
            }
            else
            {
                HotelPhone.Text = dv.Rows[0]["Hotel_Phone"].ToString();
            }
            //HotelPhone.Text = dv.Rows[0]["Hotel_Phone"].ToString();
            RoomName.Text = dv.Rows[choosenRoomId]["Room_Name"].ToString();
            string roomRefund = dv.Rows[choosenRoomId]["Cancellation_Deadline"].ToString();
            if (roomRefund == null || roomRefund.Trim() == "")
            {
                RoomRefund.Text = "Refundable";
            }
            else
            {
                RoomRefund.Text = "Non Refundable";

            }
            RoomMeal.Text = dv.Rows[choosenRoomId]["Meal_Name"].ToString();
            HotelImage.Src = Session["HotelImg"].ToString();
            //TotalPrice.Text = dv.Rows[0]["Room_TotalPrice"].ToString();
            //TotalPrice.Text = dv.Rows[0]["Room_TotalPrice"].ToString();
            //TotalPrice.Text = dv.Rows[0]["Room_TotalPrice"].ToString();
            //TotalPrice.Text = dv.Rows[0]["Room_TotalPrice"].ToString();
            //TotalPrice.Text = dv.Rows[0]["Room_TotalPrice"].ToString();
            //TotalPrice.Text = dv.Rows[0]["Room_TotalPrice"].ToString();

            //// hotel data
            //dv.Columns.Add("Hotel_Name");
            //dv.Columns.Add("Hotel_Category");
            //dv.Columns.Add("Hotel_Address");
            //dv.Columns.Add("Hotel_Phone");

            //// room data
            //dv.Columns.Add("Room_Cancel");
            //dv.Columns.Add("Room_Meal");
            //dv.Columns.Add("Room_Price");
            dv.Columns.Add("Hotel_Amenities");
            int numAmen;
            string nodeName;
            XmlDocument xdocItem = new XmlDocument();
            xdocItem.Load(AppDomain.CurrentDomain.BaseDirectory + "Cache/GetItemDetails/" + Session["HotelIDAmenities"].ToString() + ".XML");

            //  var xmls = xDoc.Root.Elements().ToArray(); // split into elements
            XmlNodeList listItem = xdocItem.SelectNodes("//HotelResult");
            //for (int i = 0; i < 21; i++)
            //{
            //    DataRow newRow = dv.NewRow();
            //    numAmen = i + 10;
            //    nodeName = listItem[0].ChildNodes[0].ChildNodes[numAmen].Name.ToString();
            //    if (nodeName != "rooms_description")
            //    {
            //        break;
            //    }
            //    else
            //    {
            //        newRow["Hotel_Amenities"] = "<i class='fa fa-check-circle circle-icon' aria-hidden='true'> " + listItem[0].ChildNodes[0].ChildNodes[numAmen].ChildNodes[0].InnerText + "</i><br />";
            //        dv.Rows.Add(newRow);
            //    }
            //}
            int hotelCatVal = int.Parse(dv.Rows[0]["Hotel_Category"].ToString());
            string stars = "";
            for (var i = 0; i < hotelCatVal; i++)
            {
                stars += "<i class='fa fa-star'></i> ";

            }
            stars += " Star/s";
            Panel pnlCont = (Panel)roomData;
            Literal lblHtlStars = (Literal)pnlCont.FindControl("lblHtlStars");
            lblHtlStars.Text = stars;
           


        

        lvAmenities.DataSource = dv;
            lvAmenities.DataBind();

            return dv;
        }

        protected void createTrvlData()
        {
            int roomsCount = int.Parse(Session["sessionNumRooms"].ToString());
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ListView containLv = (ListView)lvAdltData;
            dt.Columns.Add("countRoom");
            for (int i = 0; i < roomsCount; i++)
            {
                DataRow dr = dt.NewRow();
                dr["countRoom"] = (i + 1);
                dt.Rows.Add(dr);
            }
            ds.Tables.Add(dt);

            lvAdltData.DataSource = ds;
            lvAdltData.DataBind();

        }
        protected void roomSelect()
        {
            // sessions

            string Session_Id = Session["SessionId"].ToString();
            string Search_Token = Session["SessionSearchToken"].ToString();

            // hotel and room ID
            string Room_Id = Request.QueryString["RoomId"].ToString();
            string Hotel_Id = Request.QueryString["HotelId"].ToString();


            string RoomInfo = multi.GetRoomData(Hotel_Id, Room_Id, Session_Id, Search_Token);



            HotelName.Text = "NA";
            //gridtrialView.DataSource = OriginalDataSet.Tables[3];
            //gridtrialView.DataBind();


          

        }
        protected string passengersStr()
        {
            string str = "";
            string typeTrv;
            string frstName;
            string lstName;
            string titlTrv;
            string birthTrv;
            string nationTrv;
            string passNumTrv;
            string passCountTrv;
            string passIssueTrv;
            string passExpTrv;
            int roomsCount = int.Parse(Session["sessionNumRooms"].ToString());
            for (int ix = 0; ix < roomsCount; ix++)
            {
                ListView containLv = (ListView)lvAdltData;
                string ctrlID = "ctrl" + ix;
                ListViewDataItem smContLvDi = (ListViewDataItem)lvAdltData.FindControl(ctrlID);
                ListView smContLv = (ListView)smContLvDi.FindControl("adult1");
                int adltRoom = int.Parse(adltPerRoom(ix));
                for (int xi = 0; xi < adltRoom; xi++)
                {
                    string ctrlIDAdlt = "ctrl" + xi;
                    ListViewDataItem smContLvAdlt = (ListViewDataItem)smContLv.FindControl(ctrlIDAdlt);
                    DropDownList smContLvDdl = (DropDownList)smContLvAdlt.FindControl("ddl1");
                    TextBox smContLvFrst = (TextBox)smContLvAdlt.FindControl("first1");
                    TextBox smContLvLast = (TextBox)smContLvAdlt.FindControl("last1");
                    TextBox smContLvNation = (TextBox)smContLvAdlt.FindControl("nationalty1");

                    typeTrv = "<Type>" + "ADT" + "</Type>";
                    titlTrv = "<Title>" + smContLvDdl.SelectedItem.Text + "</Title>";
                    frstName = "<Firstname>" + smContLvFrst.Text + "</Firstname>";
                    lstName = "<Lastname>" + smContLvLast.Text + "</Lastname>";
                    birthTrv = "<Birthdate>" + "1973-07-23" + "</Birthdate>";
                    nationTrv = "<Nationality>" + smContLvNation.Text + "</Nationality>";
                    passNumTrv = "<PassportNumber>" + "4545" + "</PassportNumber>";
                    passCountTrv = "<PassportCountry>" + "Egypt" + "</PassportCountry>";
                    passIssueTrv = "<PassportIssued>" + "2015-05-13" + "</PassportIssued>";
                    passExpTrv= "<PassportExp>" + "1922-05-13" + "</PassportExp>";
                    str += "<Passenger>"+ typeTrv + titlTrv + frstName + lstName + birthTrv + nationTrv + passNumTrv + passCountTrv + passIssueTrv  + passExpTrv + "</Passenger>";
                }

            }
                //        < Passenger >
                //< Type > ADT </ Type >< !--ADT, CHD, INF-- >
      
                  //    < Title > Mrs </ Title >
      
                  //    < Firstname > Jane </ Firstname >
      
                  //    < Lastname > Doe </ Lastname >
      
                  //    < Birthdate > 1973 - 07 - 23 </ Birthdate >
      
                  //    < Nationality > egypt </ Nationality >
      
                  //    < PassportNumber > 4545 </ PassportNumber >
      
                  //    < PassportCountry > egypt </ PassportCountry >
      
                  //    < PassportIssued > 2015 - 05 - 13 </ PassportIssued >
      
                  //    < PassportExp > 2022 - 05 - 13 </ PassportExp >
      
                  //</ Passenger >
            return str;
        }







        protected void btnInit_Click(object sender, EventArgs e)
        {
         

            ////contact information
            //string contactAdd = contactAddress.Text;
            //string contactCit = contactCity.Text;
            //string contactZipCode = contactZip.Text;
            //string contactPhon = contactPhone.Text;
            //string contactEmails = contactEmail.Text;
            //string contactCountries = contactCountry.Text;

            // company data

         

            // sessions

            string Session_Id = Session["SessionId"].ToString();
            string Search_Token = Session["SessionSearchToken"].ToString();

            // passengers data

            string responseFromServerItem = multi.initbooking("", "", Session_Id, Search_Token, passengersStr(), "1491");


            //Write to Text File Caching
            System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "Cache/HotelInitBooking_" + Session_Id + ".XML", responseFromServerItem);


            //Split Rooms Using XML Nodes
            XmlDocument xdocItem = new XmlDocument();
            xdocItem.Load(AppDomain.CurrentDomain.BaseDirectory + "Cache/HotelInitBooking_" + Session_Id + ".XML");

            //  var xmls = xDoc.Root.Elements().ToArray(); // split into elements
            XmlNodeList listItem = xdocItem.SelectNodes("//InitBooking");

            DataTable dv = new DataTable();

            dv.Columns.Add("Hotel_ID");
            dv.Columns.Add("bookingitemid");

            dv.Columns.Add("total_Price");
            dv.Columns.Add("BookingId");

            for (int i = 0; i < listItem.Count; i++)
            {
                DataRow newRow1 = dv.NewRow();
                newRow1["Hotel_ID"] = listItem[i].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].InnerText;
                newRow1["bookingitemid"] = listItem[i].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[4].InnerText;
                newRow1["total_Price"] = listItem[i].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[6].ChildNodes[5].InnerText;
                newRow1["BookingId"] = listItem[i].ChildNodes[2].InnerText;
                //
                //    string sqlQuery = "INSERT INTO [dbo].[Hotel_InitBooking] (Hotel_ID, bookingitemid, total_Price, BookingId, SessionId, SearchToken) VALUES (" + newRow1["Hotel_ID"].ToString() + "," + newRow1["bookingitemid"].ToString() + "," + decimal.Parse(newRow1["total_Price"].ToString()) + "," + newRow1["BookingId"].ToString() + "," + SessionId + "," + SearchToken + ")";
                //    xtools.SQLINSERT(sqlQuery);
                //    dv.Rows.Add(newRow1);
                dv.Rows.InsertAt(newRow1, 3);
            }
            grid1.DataSource = dv;
            grid1.DataBind();
        }








        void BindHotelItemLv()
        {
            string text = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "Cache/GetItemDetails/" + Session["HotelIDAmenities"].ToString() + ".XML");

            StringReader theReader = new StringReader(text);
            DataSet originalDataSet = new DataSet("dataSet");
            originalDataSet.ReadXml(theReader);
            lvAmenities.DataSource = originalDataSet.Tables["HotelResult"];
            lvAmenities.DataBind();


        }
        protected DataSet roomCounter(int rms)
        {
            //int roomsCount = int.Parse(Session["sessionNumRooms"].ToString());
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            dt.Columns.Add("count");
            //dt.Columns.Add("countRoom");
            for (int i = 0; i < int.Parse(adltPerRoom(rms)); i++)
            {
                DataRow dr = dt.NewRow();
                dr["count"] = (i + 1);
                //dr["countRoom"] = (i + 1);
                dt.Rows.Add(dr);
            }
            ds.Tables.Add(dt);
            return ds;
        }
        int x ;
        protected string adltPerRoom(int rms)
        {
            int roomsCount = int.Parse(Session["sessionNumRooms"].ToString());
            string strAdlts = Session["sessionNumAdlts"].ToString();
            string[] str = new string[roomsCount];
            for (int i = 0; i < roomsCount; i++)
            {
                int y = (13 * i) + 9 ;
                str[i] = strAdlts.Substring(y, 1);
            }

            return str[rms];


        }
        protected void lvAdltData_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            int roomsCount = int.Parse(Session["sessionNumRooms"].ToString());

            ListView containLv = (ListView)lvAdltData;
            lvAdltData.DataBind();
            
            
                if (x == null)
                {
                    x = 0;
                }
                if (x >= roomsCount)
                {
                    //x = roomsCount-1;
                    return;
                }
                string ctrlID = "ctrl" + x;
                ListViewDataItem smContLvDi = (ListViewDataItem)lvAdltData.FindControl(ctrlID);
                //string smContLvID = "adult1_" + x;
                ListView smContLv = (ListView)smContLvDi.FindControl("adult1");
                 //smContLv = (ListView)item;
               
                
                smContLv.DataSource = roomCounter(x);
                smContLv.DataBind();
                x++;
            

            

        }
        protected void sbmtTest_Click(object sender, EventArgs e)
        {
            passengersStr();
        }
    }
}