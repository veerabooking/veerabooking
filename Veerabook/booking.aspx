﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" EnableEventValidation="false" EnableViewState="true" ValidateRequest="false" AutoEventWireup="true" CodeBehind="booking.aspx.cs" Inherits="Veerabook.booking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    Booking 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHeader" runat="server">
    

        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/loader-style.css">
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <link rel="stylesheet" href="assets/css/profile.css">

    <style>
        .pull-left {
            color:#0d4976;
        }
         .pull-left -span{
             color:black !important;
         }
         .ui-select{
             margin-top:17px;
         }
         .body-nest{
             height:100%
         }
        body .TravelersData {
            margin-bottom:20px;
        }
        .profile-name {
         min-height: 428px !important;
        }
        .list-group-item{
            height:40px;
        }
        .star-icon{
            float:right;
        }
        .htlImg {
        height:200px !important;
        }
        .form-control {
            color: #000 !important;
            border: 1px solid #ccc !important;
        }
    </style>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
<%--    <asp:GridView runat="server" ID="gridtrialView"></asp:GridView>--%>
    <%-- MY HOTEL --%>
    <div class="col-sm-12">
        <div class="well profile">
            <div class="col-sm-12">
                <asp:Panel ID="roomData" runat="server">
                    <div class="col-xs-12 col-sm-4 text-center">

                        <ul class="list-group">
                            <li class="list-group-item text-left">
                                <span class="maki-commerical-building"></span>&nbsp;&nbsp;<asp:Literal ClientIDMode="Static" ID="HotelName" Text="HotelName" runat="server" /></li>
                            <li class="list-group-item text-center htlImg">
                                <img src="http://api.randomuser.me/portraits/men/10.jpg" id="HotelImage" runat="server" alt="" class="img-circle img-responsive img-profile">

                            </li>
                            <li class="list-group-item text-center">
                                <span class="pull-left">
                                    <strong>Ratings</strong>
                                </span>

                            <div class="star-icon">
                                <!-- ngIf: hoteldetail.HotelRating == 'Special Category' -->
                                <!-- ngRepeat: n in range(hoteldetail.HotelRating) -->
                                <%--<i class="fa fa-star star-icon-padding ng-scope" aria-hidden="true"></i>--%>
                                <!-- end ngRepeat: n in range(hoteldetail.HotelRating) -->
                                <%--<i class="fa fa-star star-icon-padding ng-scope" aria-hidden="true"></i>--%>
                 
                                    <asp:Literal runat="server" id="lblHtlStars"> </asp:Literal> 
                

                                <!-- end ngRepeat: n in range(hoteldetail.HotelRating) -->
                            </div>

                            </li>

                            <li class="list-group-item text-right">
                                <span class="pull-left">
                                    <strong>CHECK IN</strong>
                                </span>
                                <span><asp:Literal ID="CheckIn" runat="server" /></span></li>

                            <li class="list-group-item text-right">
                                <span class="pull-left">
                                    <strong>CHECK OUT</strong>
                                </span><span><asp:Literal ID="CheckOut" runat="server" /></span></li>
                            <li class="list-group-item text-right">
                                <span class="pull-left">
                                <strong>TOTAL NIGHTS</strong>
                                </span><asp:Literal ID="TotalNights" runat="server" /></li>
                            <li class="list-group-item text-right">
                                <span class="pull-left">
                                <strong>TOTAL PRICE</strong>
                                </span><asp:Literal ID="TotalPrice" runat="server" /></li>


                        </ul>

                    </div>
                    <div class="col-xs-12 col-sm-8 profile-name">
                    <h2><asp:Literal ID="RoomName" runat="server" />
                        <span style="display:none;" class="pull-right social-profile">
                            <i class="entypo-facebook-circled"></i>  <i class="entypo-twitter-circled"></i>  <i class="entypo-linkedin-circled"></i>  <i class="entypo-github-circled"></i>  <i class="entypo-gplus-circled"></i>
                        </span>
                    </h2>
                    <hr>
                                    
                        <dl class="dl-horizontal-profile">
                            <dt>Hotel Name</dt>
                            <dd><asp:Literal ID="HotelTextName" runat="server" /></dd>

                            <dt>Address</dt>
                            <dd><asp:Literal ID="HotelAddress" runat="server" /></dd>
                                            
                            <dt>Phone</dt>
                            <dd><asp:Literal ID="HotelPhone" runat="server" /></dd>

                            <dt>Refundable</dt>
                            <dd class="tags danger"><asp:Literal ID="RoomRefund" runat="server" /></dd>

                            <dt>Meal</dt>
                            <dd><asp:Literal ID="RoomMeal" runat="server" /></dd>

                            <dt>Room Amenities</dt>
                            <dd style="column-count:3;">
                                <asp:ListView ID="lvAmenities" runat="server">
                                    <ItemTemplate>
                                        <p><%#Eval("Hotel_Amenities") %></p>
                                    </ItemTemplate>
                                </asp:ListView>
                            </dd>

                            <%--<dt>Hobbies</dt>
                            <dd>Read, out with friends, listen to music, draw and learn new things</dd>

                            <dt>Skills</dt>
                            <dd>
                                <span class="tags">html5</span>
                                <span class="tags">css3</span>
                                <span class="tags">jquery</span>
                                <span class="tags">bootstrap3</span>
                            </dd>--%>
                            </dl>
                                    

                </div>
                </asp:Panel>
                <br />
                <asp:GridView ID="gvRoomData" hidden="true" runat="server"></asp:GridView>
                                
                <div class="col-lg-12 col-xs-12">


                </div>
            </div>
            <div class="col-xs-12 divider text-center">
                <div class="col-xs-12 col-sm-4 emphasis">
                    <h2>
                        <strong>20,7K</strong>
                    </h2>
                    <p>
                        <small>Followers</small>
                    </p>
                    <button class="btn btn-success btn-block">
                        <span class="fa fa-plus-circle"></span>&nbsp;&nbsp;Follow</button>
                </div>
                <div class="col-xs-12 col-sm-4 emphasis">
                    <h2>
                        <strong>245</strong>
                    </h2>
                    <p>
                        <small>Following</small>
                    </p>
                    <button class="btn btn-info btn-block">
                        <span class="fa fa-user"></span>&nbsp;&nbsp;View Profile</button>
                </div>
                <div class="col-sm-4 emphasis">
                    <h2>
                        <strong>43</strong>
                    </h2>
                    <p>
                        <small>Likes</small>
                    </p>
                    <button class="btn btn-default btn-block">
                        <span class="fa fa-user"></span>&nbsp;&nbsp;Likes</button>
                </div>
            </div>
        </div>
    </div>
    <%--Room & Travelers Details--%>
    <div class="TravelersData">
        <!-- BLANK PAGE-->

        <div style="margin:-20px 15px;" class="nest" id="Blank_PageClose">
            <div class="title-alt">
                <h6>
                    Room & Travelers Details</h6>
                <div class="titleClose">
                    <a class="gone" href="#Blank_PageClose">
                        <span class="entypo-cancel"></span>
                    </a>
                </div>
                <div class="titleToggle">
                    <a class="nav-toggle-alt" href="#Blank_Page_Content"><span class="entypo-up-open"></span></a>
                </div>

            </div>

            <div class="body-nest" id="Blank_Page_Content" style="display: block;">




                <div class="row">
                    <!-- left column -->
                                        
                    <asp:ListView ID="lvAdltData" OnItemDataBound="lvAdltData_ItemDataBound" ClientIDMode="Static" runat="server">
                        <ItemTemplate>
                            <div class="col-lg-12">
                                <asp:Label runat="server" ID="lblRoomTitle"> Room Number <%# Eval("countRoom") %></asp:Label>
                            </div>
                            <asp:ListView ID="adult1" runat="server">
                                <ItemTemplate>
                                    <%-- 1 --%>
                                    <div class="col-lg-12">
                                    <%-- ddl For type person --%>
                                    <div class="col-lg-12">
                                        <asp:Label runat="server" ID="lblAdltCount"> Adult Number <%# Eval("count") %></asp:Label>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="ui-select">
                                            <asp:DropDownList ID="ddl1" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="-1"> Title</asp:ListItem>
                                                <asp:ListItem Value="mr">MR</asp:ListItem>
                                                <asp:ListItem Value="ms">MS</asp:ListItem>
                                                <asp:ListItem Value="mrs">MRS</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <%-- FirstName --%>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"></label>
                                            <asp:TextBox runat="server" placeholder="FirstName" ID="first1" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <%-- LastName --%>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"></label>
                                            <asp:TextBox runat="server" placeholder="LastName" ID="last1" CssClass="form-control"></asp:TextBox>

                                        </div>
                                    </div>
                                    <%-- Nationalty --%>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"></label>
                                            <asp:TextBox runat="server" placeholder="Nationalty" ID="nationalty1" CssClass="form-control"></asp:TextBox>

                                        </div>
                                    </div>
                                </div>
                                    <br />
                                    <hr />
                                </ItemTemplate>
                            </asp:ListView>
                        </ItemTemplate>
                    </asp:ListView>
                    <!-- edit form column -->

<%--                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-8">
                                    <input class="btn btn-primary" value="Save Changes" type="button">
                                    <span></span>
                                    <input class="btn btn-default" value="Cancel" type="reset">
                                </div>
                            </div>--%>
                                            
                    </div>
                <asp:Button Text="submit" ID="sbmtTest" CssClass="btn btn-info" OnClick="sbmtTest_Click" runat="server" />
                </div>
            </div>
    </div>
    <%-- Special Request If Any --%>
    <div class="col-sm-12">

        <div class="nest" id="buttonClose" >
            <div class="title-alt">
                <h6>
                    Special Request If Any</h6>
                <div class="titleClose">
                    <a class="gone" href="#buttonClose">
                        <span class="entypo-cancel"></span>
                    </a>
                </div>
                <div class="titleToggle">
                    <a class="nav-toggle-alt" href="#button">
                        <span class="entypo-up-open"></span>
                    </a>
                </div>

            </div>

            <div class="body-nest" id="button" style ="min-height :200px;">
                                

                <div class="col-lg-3">

                <asp:CheckBox ID="CheckBox1" runat="server" />
                <asp:Label>King Size Bed</asp:Label><br />
                <asp:CheckBox ID="CheckBox2" runat="server" />
                <asp:Label>Early Check In</asp:Label><br />
                <asp:CheckBox ID="CheckBox3" runat="server" />
                <asp:Label>Smoking</asp:Label ><br />
                    <asp:CheckBox ID="CheckBox4" runat="server" />
                <asp:Label>Baby Cot</asp:Label><br />
                </div>
                <div class="col-lg-3">

                <asp:CheckBox ID="CheckBox5" runat="server" />
                <asp:Label>Twin Beds</asp:Label><br />
                <asp:CheckBox ID="CheckBox6" runat="server" />
                <asp:Label>Late Check Out</asp:Label><br />
                <asp:CheckBox ID="CheckBox7" runat="server" />
                <asp:Label>NON Smoking</asp:Label ><br />
                    <asp:CheckBox ID="CheckBox8" runat="server" />
                <asp:Label>Rooms Next To Each Other</asp:Label><br />
                </div>
                <div class="col-lg-3">

                <asp:CheckBox ID="CheckBox9" runat="server" />
                <asp:Label>Honeymooners</asp:Label><br />
                <asp:CheckBox ID="CheckBox10" runat="server" />
                <asp:Label>High Floor</asp:Label><br />
                <asp:CheckBox ID="CheckBox11" runat="server" />
                <asp:Label>Near Elevator</asp:Label ><br />
                    <asp:CheckBox ID="CheckBox12" runat="server" />
                <asp:Label>VIP Client</asp:Label><br />
                </div>
                <div class="col-lg-3">

                <asp:CheckBox ID="CheckBox13" runat="server" />
                <asp:Label>Ground Floor</asp:Label><br />
                <asp:CheckBox ID="CheckBox14" runat="server" />
                <asp:Label>Connected Rooms</asp:Label><br />
                </div>
                <div class="col-lg-12">
                            <div class="form-group">
                            <label for="exampleInputEmail1"></label>
                            <input type="email" placeholder="Please enter any special requests that you may have late check in (Please enter any special requests that you may have late check in ")id="speacilrequst" class="form-control">
                                <br />
                                <asp:Label> We will pass your special requests to your hotel but please note that we cannot guarantee these requests, and they may incur additional charges</asp:Label>
                        <br />

                            </div>
                        </div>
        </div>
    </div>
                  
    </div>
    <%-- contact information --%>
    <div class="col-sm-12">
        <div class="nest" id="horizontalClose">
            <div class="title-alt">
                <h6>
                    Contact information</h6>
                <div class="titleClose">
                    <a class="gone" href="#horizontalClose">
                        <span class="entypo-cancel"></span>
                    </a>
                </div>
                <div class="titleToggle">
                    <a class="nav-toggle-alt" href="#horizontal"><span class="entypo-down-open"></span></a>
                </div>

            </div>

            <div class="body-nest" id="horizontal" >

                <div class="form_center">
                    <form role="form" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-8 control-label" for="inputEmail1">Contact</label>
                            <div class="col-lg-10">
<%--                                                <input type="email" placeholder="Email" id="inputEmail1" class="form-control">--%>
                                <asp:TextBox runat="server" ID="contactAddress" CssClass="form-control" placeholder="Address" />
                                <asp:TextBox runat="server" ID="contactCity" CssClass="form-control" placeholder="City" />
                                <asp:TextBox runat="server" ID="contactCountry" CssClass="form-control" placeholder="Country" />
                                <asp:TextBox runat="server" ID="contactZip" CssClass="form-control" placeholder="Zip Code" />
                                <asp:TextBox runat="server" ID="contactPhone" CssClass="form-control" placeholder="Phone Number" />
                                <asp:TextBox runat="server" ID="contactEmail" CssClass="form-control" placeholder="Email" />
                                <p class="help-block">Confirmation will be sent to this email address</p>
                            </div>
                        </div>
                                        
                                        
                        <div class="form-group">
                                            
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>
    <%-- Payment method --%>
    <div class="col-sm-12">
        <div class="nest" id="elementClose">
            <div class="title-alt">
                <h6>
                    Payment method</h6>
                <div class="titleClose">
                    <a class="gone" href="#elementClose">
                        <span class="entypo-cancel"></span>
                    </a>
                </div>
                <div class="titleToggle">
                    <a class="nav-toggle-alt" href="#element">
                        <span class="entypo-up-open"></span>
                    </a>
                </div>

            </div>

            <div class="body-nest" id="element">

                <div class="panel-body">
                                    
                        <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <span class="entypo-attention"></span>
                    <strong>Warring</strong>&nbsp;&nbsp;There is no paymode available!
                </div>
                                    
                </div>

            </div>

        </div>
    </div>
    <%-- Terms and conditions --%>
        <div class="col-sm-12">
            <div class="nest" id="inlineClose">
                <div class="title-alt">
                    <h6>
                        Terms and conditions</h6>
                    <div class="titleClose">
                        <a class="gone" href="#inlineClose">
                            <span class="entypo-cancel"></span>
                        </a>
                    </div>
                    <div class="titleToggle">
                        <a class="nav-toggle-alt" href="#inline">
                            <span class="entypo-up-open"></span>
                        </a>
                    </div>

                </div>

                <div class="body-nest" id="inline" style ="height :70px;">

                    <div class="form_center">
                                    
                                        
                            <div class="col-lg-12">
                                            
                                <div class="checkbox">
                                <label>
                                    <input type="checkbox">I accept the following terms and conditions:

                                </label>
                            </div>
                            </div>
                                        
                                        
                                    
                    </div>

                </div>

            </div>
        </div>
    <%-- Total Due --%>
    <div class="col-sm-12">

        <div class="nest" id="alertClose">
            <div class="title-alt">
                <h6>
                    Total Due</h6>
                <div class="titleClose">
                    <a class="gone" href="#alertClose">
                        <span class="entypo-cancel"></span>
                    </a>
                </div>
                <div class="titleToggle">
                    <a class="nav-toggle-alt" href="#alert"><span class="entypo-down-open"></span></a>
                </div>

            </div>

            <div class="body-nest" id="alert" style="padding-bottom: 120px;">

                <div class="alert alert-info">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <span class="entypo-info-circled"></span>
                    <strong>Change currency</strong>&nbsp;&nbsp;Gross price total 
                    <strong>( 100.112 USD)</strong>
                </div>
                <div id="notif" style="display:block" class="body-nest">

<%--                                <button  class="btn source" data-original-title="" title="">Book Now</button>--%>
                    <asp:Button Text="Book Now" CssClass="btn source" OnClick="btnInit_Click" ID="btnInit" runat="server" />
                    <asp:GridView ID="grid1" runat="server" ></asp:GridView>
                    <br />
                    <hr />
                    <asp:GridView ID="gvSubmtBook" runat="server" ></asp:GridView>

                </div>
            </div>
        </div>
    </div>
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
   

</asp:Content>
