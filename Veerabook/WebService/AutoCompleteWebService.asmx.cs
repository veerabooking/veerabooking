﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using Veerabook;

namespace Veerabook
{
    /// <summary>
    /// Summary description for AutoCompleteWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]

   
    public class AutoCompleteWebService : System.Web.Services.WebService
    {
        private Xtools _xtools;

        /// <summary>
        /// Gets the random strings.
        /// </summary>
        /// <param name="Value">The value.</param>
        /// <returns>GetRandomStringsResult)</returns>
        /// <author>Luca Congiu (25/05/2013)</author>
        /// <includesource>yes</includesource>
        public string CityString(string name)
        {
            return  name;
        }
        protected void login()
        {
            WebRequest request = WebRequest.Create("http://api-test.multireisen.com/login/");
            request.Method = "POST";
            string postData = "xml=" + "<MultireisenAPI_Request><Login><LoginName>admin@dynawix.com</LoginName><Passw>123</Passw></Login></MultireisenAPI_Request>";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse response = request.GetResponse();

            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            string responseFrmSrv = responseFromServer;
            StringReader theReader = new StringReader(responseFrmSrv);
            DataSet OriginalDataSet = new DataSet("dataSet");

            OriginalDataSet.ReadXml(theReader);


            //Session["SessionId"] = OriginalDataSet.Tables["Login"].Rows[0]["SessionID"];
            Session["SessionId"] = "pu6umpb0c6c4foai3qjs5vk5i0";



            reader.Close();
            dataStream.Close();
            response.Close();

        }
        public DataTable GetCities(string name)
        {
           

            return _xtools.Findany("Cities", "city_name_en=" + name);
        }

        [WebMethod()]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<GetRandomStringsResult> GetRandomStrings(string name, string Value)
        {
            
            List<GetRandomStringsResult> result = new List<GetRandomStringsResult>();
            //Generate Random Strings
            //login();
            //.Columns["Name"]
            DataTable myCities =  GetCities(name);
            int i = 0;
            foreach (DataRow row in myCities.Rows)
            {
                result.Add(new GetRandomStringsResult(row["city_name_en"].ToString() , i.ToString()));
                i++;
            }

            //List<GetRandomStringsResult> list = (from row in myCities.AsEnumerable() select row.Field<GetRandomStringsResult>("city_name_en")).ToList<GetRandomStringsResult>();

            //result = list;
            return result;

        }
    }

    /// <summary>
    /// Result Class for json
    /// </summary>
    /// <author>LCO (25/05/2013)</author>
    /// <includesource>yes</includesource>
    public class GetRandomStringsResult
    {

        public GetRandomStringsResult()
        {
        }
        public GetRandomStringsResult(string name, string value)
        {
            this.Name = name;
            this.Value = value;
            
        }
        public string Name { get; set; }
        public string Value { get; set; }


    }



}
