﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="WebForm4.aspx.cs" Inherits="Veerabook.WebForm4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHeader" runat="server">
    <style>

        .body-nest{
            min-height:150px;
        }
                #amount {
            background-color:initial !important;
        }
        body #CPContent_ddlStarRatingFilter > .chechCat {
            
        }
        body #CPContent_ddlStarRatingFilter > .starCat {
            font-size: 20px !important;
            font-style: italic;
        }
        body #CPContent_ddlStarRatingFilter > .fa-star {
            font-size: 15px !important;
            font-style:italic !important;
            color:#ddcf34;
        }
        .dropdown-toggle{
            margin-top:10px;
        }
    </style>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.slider.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui-1.9.1.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">

                <div  class="container-fluid">
                <div class="row">


                    <div class="col-sm-12">
                        <!-- BLANK PAGE-->

                        <div class="nest" id="Blank_PageClose">
                            <div class="title-alt">
                                <h6>
                                    Filters</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#Blank_PageClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Blank_Page_Content">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Blank_Page_Content">
                                <%-- Enter hotel name  --%>
                                <div class="form-group col-lg-6 col-sx-12">
                                    
                                            <label for="exampleInputEmail2" class="sr-only"></label>
                                            <input type="email" placeholder="Enter the hotel name " id="lblhotelname" class="form-control">
                                       

                            </div>

                                <%-- Enter Loctaion --%>
                               <div class="form-group col-lg-6 col-sx-12 ">
                                
                                            <label for="exampleInputEmail2" class="sr-only"></label>
                                            <input type="email" placeholder="Enter the location " id="lblloc" class="form-control">
                                           </div>
                                 <br /> 
                                <br />
                                
                                <%-- START star rating --%>
                                <div class="col-lg-6">
                                    <asp:DropDownList ID="DropDownList1" CssClass="btn btn-default dropdown-toggle col-lg-6" runat="server">
                                    <asp:ListItem Text="1 Star" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="2 Stars" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="3 Stars" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="4 Stars" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="5 Stars" Value="5"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>


                                <%-- Price Range --%>
                                
                                <div class="col-lg-6 col-sx-12">
                                            <label for="amount">PRICE RANGE:</label> 
                                    <input type="text" id="amount" style="border:0; color:#f6931f; font-weight:bold;" />
                                    <div id="slider-range"></div>
                                    
                                           </div>       
                            </div>
                                
                        </div>
                    </div>
                    <!-- END OF BLANK PAGE -->


                </div>
                    </div>
                   
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
    <script>
        $(function () {
            $("#slider-range").slider({
                range: true,
                min: 50,
                max: 50000,
                values: [75, 50000],
                slide: function (event, ui) {
                    $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
                }
            });
            $("#amount").val("$" + $("#slider-range").slider("values", 0) +
                " - $" + $("#slider-range").slider("values", 1));
        });
    </script>
</asp:Content>
