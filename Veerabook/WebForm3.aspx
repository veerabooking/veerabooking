﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master"  AutoEventWireup="true" EnableEventValidation="false" ValidateRequest="false" CodeBehind="WebForm3.aspx.cs" Inherits="Veerabook.WebForm3"%>
<%@ Register TagPrefix="cc1" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=18.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    rooms Triala
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHeader" runat="server">
    <link href="css/listviewStyles.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-1.11.2.js" ></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" ></script>
    <link  href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/js/datepicker/datepicker.css">
    <link rel="stylesheet" href="assets/js/datepicker/bootstrap-datetimepicker.css">
        
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">

    <script type="text/javascript" src="assets/js/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="assets/js/wizard/lib/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
  
    <style>
        .marginRightSm {
            margin-right:8px;
        }
        .input-group[class*="col-"] {
            float: left !important;
        }
        .body-nest {
            min-height:150px !important;
        }
        #numRooms {
            margin-left:15px ;
        }
        .ddlListItem {
            float: none !important;
            width: 100%;
        }
    </style>

 
<%-- another   fake update--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <div class="col-xs-12">
        <div class="row col-xs-12 noPaddingMediaXS">
            <div class="col-xs-12 marginTop roomsHeader">
                <div class="col-xs-3 text-center headerItem">Room Types</div>
                <div class="col-xs-1 text-center headerItem">Pax</div>
                <div class="col-xs-3 text-center headerItem">Inclusions</div>
                <div class="col-xs-3 text-center headerItem">Price</div>
                <div class="col-xs-2 text-center headerItem"></div>
            </div>
        </div>
        <div class="row col-xs-12 lvRoomsCont">
            <div class="row paddingSide">
                <%--try an update--%>
                <%--<asp:ListView ID="lvRooms" runat="server">
                <ItemTemplate>--%>
                <div class="col-xs-12 lvRoomsRow">
                    <div class="col-xs-3 text-center dataItem">Standard Room</div>
                    
                    <div class="col-xs-1 text-center dataItem roomCapacity">
                        <i class="fa fa-user"></i>
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="col-xs-3 text-center dataItem">Room Only</div>
                    <div class="col-xs-3 text-center dataItem">USD 70.00</div>
                    <div class="col-xs-2 text-center buttonItem">
                        <asp:Button id="btnsubmitRoom" CssClass="col-xs-12 btn btn-primary" Text="Book" runat="server"/>
                    </div>
                </div>
                <div class="col-xs-12 lvRoomsRow">
                    <div class="col-xs-3 text-center dataItem">Standard Room</div>
                    <div class="col-xs-1 text-center dataItem roomCapacity">
                        <div class="uIco ">
                            <i class="fa fa-user"></i>
                            <i class="fa fa-user"></i>
                        </div>
                    </div>
                    <div class="col-xs-3 text-center dataItem">Room Only</div>
                    <div class="col-xs-3 text-center dataItem">USD 70.00</div>
                    <div class="col-xs-2 text-center buttonItem">
                        <asp:Button id="Button1" CssClass="col-xs-12 btn btn-primary" Text="Book" runat="server"/>
                    </div>
                </div>
                <%--</ItemTemplate>
            </asp:ListView>--%>
            </div>
        </div>  
    </div>
<div class="content-wrap col-lg-8"  >
    <div class="row">
        <div class="col-sm-12">
            <div class="nest" id="inlineClose">
                <div class="title-alt">
                    <h6>
                        CHANGE DATES</h6>
                    <div class="titleClose">
                        <a class="gone" href="#inlineClose">
                            <span class="entypo-cancel"></span>
                        </a>
                    </div>
                    <div class="titleToggle">
                        <a class="nav-toggle-alt" href="#inline">
                            <span class="entypo-up-open"></span>
                        </a>
                    </div>

                </div>

                <div class="body-nest" id="inline">

                    <div class="form_center  col-sm-12">
                        <div class="form-inline row">
                            <asp:TextBox id="xresult" runat="server" Rows="12" Width="100%" style="display:none !important;" ></asp:TextBox>
                            <div class="form-group" >

                                <%--<input type="email" placeholder="Check In" id="exampleInputEmail32" class="form-control" runat="server">
                                            <i style="font-style:normal;" data-time-icon="entypo-clock" data-date-icon="entypo-calendar" class="entypo-calendar"></i>--%>
                                <div id="demo" class="col-sm-4 input-group date marginRightSm">
                                    <asp:TextBox class="form-control" placeholder="Check In & out " runat="server" id="startDate"  ></asp:TextBox>
                                    <asp:TextBox class="form-control" runat="server" id="chckIn" style="display: none !important;" ></asp:TextBox>
                                    <asp:TextBox class="form-control" runat="server" id="chckOut" style="display: none !important;" ></asp:TextBox>
                                    <span class="input-group-addon add-on">
                                        <i style="font-style:normal;" data-time-icon="entypo-clock" data-date-icon="entypo-calendar" class="entypo-calendar"></i>
                                    </span>
                                </div>
                                <%--<div id="demo1" class="col-sm-2 input-group date marginRightSm">
                                                <asp:TextBox class="form-control " placeholder="Check Out" runat="server" id="txtCheckOut" data-format="yyyy-MM-dd" type="text" ></asp:TextBox>
                                                    <span class="input-group-addon add-on">
                                                    <i style="font-style:normal;" data-time-icon="entypo-clock" data-date-icon="entypo-calendar" class="entypo-calendar"></i>
                                                </span>
                                            </div>
                                        --%>
                                <%--<label for="exampleInputPassword2" class="sr-only">Password</label>--%>
                                <%--<div class="btn-group col-sm-3">
                                                <button data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" type="button">Please Choose Rooms
                                                    <span class="caret"></span>
                                                </button>
                                                <ul role="menu" class="dropdown-menu" id="numRooms">
                                                    <li><a href="#">1</a>
                                                    </li>
                                                    <li><a href="#">2</a>
                                                    </li>
                                                    <li><a href="#">3</a>
                                                    </li>
                                                </ul>
                                            </div>--%>
                                <div class="col-sm-3 input-group marginRightSm">
                                    <asp:DropDownList runat="server" ID="ddlNoRooms" AutoPostBack="False"  CssClass="btn btn-secondary col-sm-3 form-control"  >
                                        <asp:ListItem Text="Please choose room" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>  
                                <div class="col-sm-2">                                          
                                    <asp:Button ID="btnSearchHotel" runat="server" CssClass="btn btn-success btnSearchHotel" Text="Check availbailaity" />
                                </div>
                                <asp:ListView ID="RoomsRpt" runat="server">
                                    <ItemTemplate>
                                        <%--<div id="roomsData" class="col-sm-4 marginTop">
                                            <asp:Label CssClass="input-group" runat="server">Number of Adults </asp:Label>
                                            <asp:DropDownList ID="ddlNoAdult" CssClass="btn btn-secondary input-group form-control marginRight ddlListItem" runat="server"AutoPostBack="False"  >
                                                <asp:ListItem Selected="true" Text="1" Value="1"></asp:ListItem>
                                                <asp:ListItem  Text="2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label CssClass="input-group" runat="server">Number of Children </asp:Label>  
                                            <asp:DropDownList ID="ddlNoChildren" CssClass="btn btn-secondary input-group form-control marginRight ddlListItem" runat="server" AutoPostBack="False">
                                                <asp:ListItem Selected="true" Text="no Children" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label CssClass="input-group" runat="server" ID="lblChild_1" Visible="false" ClientIDMode="AutoID">Child 1 </asp:Label> 
                                            <asp:DropDownList ID="Child_1" Visible="false" CssClass="btn btn-secondary input-group form-control marginRight ddlListItem" placeholder="from 2 to 12" runat="server"  AutoPostBack="False" ClientIDMode="AutoID">
                                                <asp:ListItem Selected="true" Text="Age 1st Child" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label CssClass="input-group" runat="server" ID="lblChild_2" Visible="false" ClientIDMode="AutoID">Child 2 </asp:Label> 
                                            <asp:DropDownList ID="Child_2" Visible="false" CssClass="btn btn-secondary input-group form-control marginRight ddlListItem"  placeholder="from 2 to 12" runat="server"  AutoPostBack="False" ClientIDMode="AutoID">
                                                <asp:ListItem Selected="true" Text="Age 2nd Child" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label CssClass="input-group" runat="server" ID="lblChild_3" Visible="false" ClientIDMode="AutoID">Child 3 </asp:Label> 
                                            <asp:DropDownList ID="Child_3" Visible="false" CssClass="btn btn-secondary input-group form-control marginRight ddlListItem"  placeholder="from 2 to 12" runat="server"AutoPostBack="False" ClientIDMode="AutoID">
                                                <asp:ListItem Selected="true" Text="Age 3rd Child" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                        </div>--%>
                                    </ItemTemplate>

                                </asp:ListView>
                                <div id="roomDataHidden" class="row" style="display:none;">
                                    <br /> <br />
                                    <asp:TextBox ID="Xroom_1" runat="server" Text="<Room><RoomType>#1#</RoomType><Children>@Children@</Children></Room>">
                                    </asp:TextBox>
                                    <br />
                                    <asp:TextBox ID="Xroom_1_Child" runat="server" Text="">
                                    </asp:TextBox>
                                    <br />
                   
             
                                    <br /> <br />

                                    <asp:TextBox ID="Xroom_2" runat="server" Text="<Room><RoomType>#1#</RoomType><Children>@Children@</Children></Room>">
                                    </asp:TextBox>
                                    <br />
                                    <asp:TextBox ID="Xroom_2_Child" runat="server" Text="">
                                    </asp:TextBox>
                                    <br />
                   
                                    <br /> <br />


                                    <asp:TextBox ID="Xroom_3" runat="server" Text="<Room><RoomType>#1#</RoomType><Children>@Childern@</Children></Room>">
                                    </asp:TextBox>
                                    <br />
                                    <asp:TextBox ID="Xroom_3_Child" runat="server" Text="">
                                    </asp:TextBox>
                                    <br />
                 
                                    <br /><br />
              
                
              
                                </div>
                    
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    
    </div>
    
    
    
    <%-- Tabs --%>
    
<div class="col-sm-12">
    <div class="nest" id="tabClose">
        <div class="title-alt">
            <h6>
                Tabs</h6>
            <div class="titleClose">
                <a class="gone" href="#tabClose">
                    <span class="entypo-cancel"></span>
                </a>
            </div>
            <div class="titleToggle">
                <a class="nav-toggle-alt" href="#tab">
                    <span class="entypo-up-open"></span>
                </a>
            </div>

        </div>

        <div class="body-nest" id="tab">

            <div id="wizard-tab">
                <h2>First Step</h2>
                <section>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut nulla nunc. Maecenas arcu sem, hendrerit a tempor quis, sagittis accumsan tellus. In hac habitasse platea dictumst. Donec a semper dui. Nunc eget quam libero. Nam at felis metus. Nam tellus dolor, tristique ac tempus nec, iaculis quis nisi.</p>
                </section>

                <h2>Second Step</h2>
                <section>
                    <p>Donec mi sapien, hendrerit nec egestas a, rutrum vitae dolor. Nullam venenatis diam ac ligula elementum pellentesque. In lobortis sollicitudin felis non eleifend. Morbi tristique tellus est, sed tempor elit. Morbi varius, nulla quis condimentum dictum, nisi elit condimentum magna, nec venenatis urna quam in nisi. Integer hendrerit sapien a diam adipiscing consectetur. In euismod augue ullamcorper leo dignissim quis elementum arcu porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum leo velit, blandit ac tempor nec, ultrices id diam. Donec metus lacus, rhoncus sagittis iaculis nec, malesuada a diam. Donec non pulvinar urna. Aliquam id velit lacus.</p>
                </section>

                <h2>Third Step</h2>
                <section>
                    <p>Morbi ornare tellus at elit ultrices id dignissim lorem elementum. Sed eget nisl at justo condimentum dapibus. Fusce eros justo, pellentesque non euismod ac, rutrum sed quam. Ut non mi tortor. Vestibulum eleifend varius ullamcorper. Aliquam erat volutpat. Donec diam massa, porta vel dictum sit amet, iaculis ac massa. Sed elementum dui commodo lectus sollicitudin in auctor mauris venenatis.
                    </p>
                </section>

                <h2>Forth Step</h2>
                <section>
                    <p>Quisque at sem turpis, id sagittis diam. Suspendisse malesuada eros posuere mauris vehicula vulputate. Aliquam sed sem tortor. Quisque sed felis ut mauris feugiat iaculis nec ac lectus. Sed consequat vestibulum purus, imperdiet varius est pellentesque vitae. Suspendisse consequat cursus eros, vitae tempus enim euismod non. Nullam ut commodo tortor.</p>
                </section>
            </div>

        </div>
    </div>
</div>


    <%-- Tabs --%>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
    <script type="text/javascript">
        var checkIn = "";
        var checkOut = "";
        var today = new Date();
        Date.prototype.addDays = function (days) {
            var dat = new Date(this.valueOf());
            dat.setDate(dat.getDate() + days);
            return dat;
        }
        //var txtdates = "From " + today + " to " + today.addDays(1);
        //$("[name='ctl00$CPContent$startDate']").val(txtdates);
        //$("[name='ctl00$CPContent$chckIn']").val(today);
        //$("[name='ctl00$CPContent$chckOut']").val(today.addDays(1));

        $('#demo').daterangepicker({
            "startDate": today,
            "endDate": today.addDays(1)
        }, function (start, end, label) {
            console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
            checkIn = start.format('YYYY-MM-DD');
            checkOut = end.format('YYYY-MM-DD');
            var dates = "From " + checkIn + " To " + checkOut ;
            $("[name='ctl00$CPContent$startDate']").val(dates);
            $("[name='ctl00$CPContent$chckIn']").val(checkIn);
            $("[name='ctl00$CPContent$chckOut']").val(checkOut);
            
            //chckIn


        });

    </script>
    <script>
        $(function() {
            $("#wizard").steps({
                headerTag: "h2",
                bodyTag: "section",
                transitionEffect: "slideLeft"
            });

            $("#wizard_vertical").steps({
                headerTag: "h2",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                stepsOrientation: "vertical"
            });

            $("#wizard-tab").steps({
                headerTag: "h2",
                bodyTag: "section",
                transitionEffect: "none",
                enableFinishButton: false,
                enablePagination: false,
                enableAllSteps: true,
                titleTemplate: "#title#",
                cssClass: "tabcontrol"
            });

            
    </script>

</asp:Content>
