﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" ValidateRequest="false" CodeBehind="WebForm2.aspx.cs" Inherits="Veerabook.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox id="txtSearchHotel" runat="server" style="display:none;" ></asp:TextBox>

        <asp:TextBox id="xresult" runat="server" TextMode="MultiLine" Rows="12" Width="100%" display="none" hidden="true"></asp:TextBox>
        <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_OnClick" />
        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_OnClick" />
        <asp:Button ID="btnInit" runat="server" Text="submit booking" OnClick="btnInit_Click" />
        <asp:GridView ID="gvHotels" runat="server" ></asp:GridView>
        <asp:GridView ID="grid1" runat="server" ></asp:GridView>
    </div>
    </form>
</body>
</html>
