﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Veerabook
{
    public partial class Loader : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RegisterAsyncTask(new PageAsyncTask(BindHotelList));
                return;
            }
        }








        public async Task BindHotelList()
        {
            WebRequest request = WebRequest.Create("http://api-test.multireisen.com/hotelsearch/");
            request.Method = "POST";
            string postData = Session["htlSrchPostData"].ToString();
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse response = request.GetResponse();

            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            //txtSearchHotel.Text = responseFromServer;

            StringReader theReader = new StringReader(responseFromServer);
            DataSet OriginalDataSet = new DataSet("dataSet");
            OriginalDataSet.ReadXmlSchema(AppDomain.CurrentDomain.BaseDirectory + "XML/Schema/MR.XSD");
            OriginalDataSet.ReadXml(theReader);

            //Get Session SearchToken to Cache With
            Session["SessionSearchToken"] = OriginalDataSet.Tables["MultireisenAPI_Response"].Rows[0]["SearchToken"];

            //Write to Text File Caching
            System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "Cache/" + Session["SessionSearchToken"] + ".XML", responseFromServer);

            //Split Rooms Using XML Nodes
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(AppDomain.CurrentDomain.BaseDirectory + "Cache/" + Session["SessionSearchToken"] + ".XML");


            //  var xmls = xDoc.Root.Elements().ToArray(); // split into elements
            XmlNodeList list = xdoc.SelectNodes("//RoomTypes");


            for (int i = 0; i < list.Count; i++)
            {


                System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "Cache/" + Session["SessionSearchToken"] + "_" + i + ".XML", list[i].OuterXml);



            }


            await Task.WhenAll();



        }



    }
}