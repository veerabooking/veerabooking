﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;

namespace Veerabook
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Xtools xtool = new Xtools();

            //Split Rooms Using XML Nodes
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load("C://Users/ETERNAL-LAP/Source/Repos/veerabooking2/Veerabook/Cache/4f13d26a8dab6c19a3b2fd3e9f9cb2af.xml");


            //  var xmls = xDoc.Root.Elements().ToArray(); // split into elements
            XmlNodeList list = xdoc.SelectNodes("//RoomType");



            var dt = new DataTable();

            dt.Columns.Add("Hotel_ID");
            dt.Columns.Add("Hotel_Name");
            dt.Columns.Add("Hotel_Category");
            dt.Columns.Add("Hotel_Address");
            dt.Columns.Add("Hotel_Phone");
            dt.Columns.Add("Hotel_City_Name");
            dt.Columns.Add("Hotel_City_Code");
            dt.Columns.Add("Hotel_Location_LAT");
            dt.Columns.Add("Hotel_Location_LNG");
            dt.Columns.Add("Room_ID");
            dt.Columns.Add("Room_Name");
            dt.Columns.Add("Meal_Code");
            dt.Columns.Add("Meal_Name");
            dt.Columns.Add("Cancellation_Deadline");
            dt.Columns.Add("Cancellation_Policy");
            dt.Columns.Add("Room_TotalPrice");
            dt.Columns.Add("Room_BasePrice");
            dt.Columns.Add("Room_BasePrice_Currency");
            dt.Columns.Add("Supplier");
            dt.Columns.Add("SessionId");
            dt.Columns.Add("SearchToken");















            for (int i = 0; i < list.Count; i++)
            {


                DataRow newRow = dt.NewRow();
                newRow["Hotel_ID"] = list[i].ParentNode.ParentNode.ChildNodes[0].InnerText;
                newRow["Hotel_Name"] = list[i].ParentNode.ParentNode.ChildNodes[1].InnerText;
                newRow["Hotel_Category"] = list[i].ParentNode.ParentNode.ChildNodes[2].InnerText;
                newRow["Hotel_Address"] = list[i].ParentNode.ParentNode.ChildNodes[3].InnerText;
                newRow["Hotel_Phone"] = list[i].ParentNode.ParentNode.ChildNodes[4].InnerText;
                newRow["Hotel_City_Name"] = list[i].ParentNode.ParentNode.ChildNodes[5].ChildNodes[0].InnerText;
                newRow["Hotel_City_Code"] = list[i].ParentNode.ParentNode.ChildNodes[5].ChildNodes[1].InnerText;
                newRow["Hotel_Location_LAT"] = list[i].ParentNode.ParentNode.ChildNodes[6].ChildNodes[0].ChildNodes[0].InnerText;
                newRow["Hotel_Location_LNG"] =  list[i].ParentNode.ParentNode.ChildNodes[6].ChildNodes[0].ChildNodes[1].InnerText;
                newRow["Room_ID"] = list[i].ChildNodes[0].InnerText;
                newRow["Room_Name"] = list[i].ChildNodes[1].InnerText;
                newRow["Meal_Code"] = list[i].ChildNodes[2].ChildNodes[0].ChildNodes[0].InnerText;
                newRow["Meal_Name"] = list[i].ChildNodes[2].ChildNodes[0].ChildNodes[1].InnerText;
                newRow["Cancellation_Deadline"] = list[i].ChildNodes[3].ChildNodes[0].InnerText;
                newRow["Cancellation_Policy"] = list[i].ChildNodes[3].ChildNodes[1].InnerText;
                newRow["Room_BasePrice"] = list[i].ChildNodes[4].ChildNodes[0].InnerText;
               newRow["Room_TotalPrice"] = list[i].ChildNodes[4].ChildNodes[5].InnerText;
               newRow["Room_BasePrice_Currency"] = list[i].ChildNodes[4].ChildNodes[6].InnerText;
                newRow["Supplier"] = "Multi";

                newRow["SessionId"] = list[i].ParentNode.ParentNode.ParentNode.ParentNode.ChildNodes[0].InnerText;
                newRow["SearchToken"] = list[i].ParentNode.ParentNode.ParentNode.ParentNode.ChildNodes[1].InnerText;



                dt.Rows.Add(newRow);













            }














            gv1.DataSource = dt;
            gv1.DataBind();

          

        }

     
        protected void ddl_SelectedIndexChanged(object sender, EventArgs e)
        {


            //DataSet dataSet = new DataSet();
            //dataSet.ReadXml("C://Users/ETERNAL-LAP/Source/Repos/veerabooking2/Veerabook/Cache/4f13d26a8dab6c19a3b2fd3e9f9cb2af.xml", XmlReadMode.InferSchema);


            //gv1.DataSource = dataSet;
            //gv1.DataBind();



            //gv1.DataSource = dataSet.Tables[ddl.SelectedIndex];
            //gv1.DataBind();
        }
    }

    public class Sequence
    {
        public Point[] SourcePath { get; set; }
    }
}