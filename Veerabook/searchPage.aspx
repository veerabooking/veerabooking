﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="searchPage.aspx.cs" Inherits="Veerabook.searchPage" EnableViewState="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<script type="text/javascript">
        $(document).ready(function () {
            $('#txtDestinations').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: 'Service.asmx/SearchCity',
                        method: 'POST',
                        contentType: 'application/json;charset-utf-8',
                        data: JSON.stringify({ prefixText: $('#txtDestinations').val(), count: 1 }),
                        dataType: 'json',
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (err) {
                            alert("No city with this name, please make sure of the spelling!");
                        }
                    });
                }
            });
        });
    </script>
</head>
<body>

    <form id="form2" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="nest" id="inlineClose">
                        <div class="title-alt">
                            <h6>Find Your Destination</h6>
                            <div class="titleClose">
                            </div>
                            <div class="titleToggle">
                                <a class="nav-toggle-alt" href="#inline">
                                    <span class="entypo-up-open"></span>
                                </a>
                            </div>

                        </div>

                        <div class="body-nest" id="inline">

                            <div class="col-sm-12">
                                <div class="form-inline row">
                                    <asp:TextBox ID="xresult" Style="width: 100%; display: none;" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="searchData" ClientIDMode="static" Style="width: 100%; display: none;" runat="server"></asp:TextBox>
                                    <div class="form-group">
                                        <asp:TextBox runat="server" ClientIDMode="Static" ID="txtDestinations" class="form-control col-sm-2 marginRightSm" placeholder="Your Destinations"></asp:TextBox>
                                        <div id="demo" class="col-sm-4 input-group date marginRightSm">
                                            <asp:TextBox ID="startDate" runat="server" class="form-control" placeholder="Check In &amp; out "></asp:TextBox>
                                            <asp:TextBox ID="chckIn" runat="server" class="form-control" Style="display: none !important;"></asp:TextBox>
                                            <asp:TextBox ID="chckOut" runat="server" class="form-control" Style="display: none !important;"></asp:TextBox>
                                            <span class="input-group-addon add-on">
                                                <i style="font-style: normal;" data-time-icon="entypo-clock" data-date-icon="entypo-calendar" class="entypo-calendar"></i>
                                            </span>
                                        </div>
                                        <div class="col-sm-3 input-group marginRightSm">
                                            <asp:DropDownList ID="ddlNoRooms" OnSelectedIndexChanged="ddlNoRooms_SelectedIndexChanged" CssClass="btn btn-secondary col-sm-3 form-control" runat="server">
                                                <asp:ListItem Text="Please Select Rooms" Value="-1" />
                                                <asp:ListItem Text="1" Value="1" />
                                                <asp:ListItem Text="2" Value="2" />
                                                <asp:ListItem Text="3" Value="3" />
                                                <asp:ListItem Text="4" Value="4" />
                                                <asp:ListItem Text="5" Value="5" />
                                                <asp:ListItem Text="6" Value="6" />
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:Button runat="server" ID="btnSearchHotel" OnClick="btnSearchHotel_Click" Text="Search" class="btn btn-success btnSearchHotel" />
                                        </div>
                                        <div class="clearfix"></div>
                                        <div id="roomData1"></div>
                                        <div id="roomData2"></div>
                                        <div id="roomData3"></div>
                                        <div id="roomData4"></div>
                                        <div id="roomData5"></div>
                                        <div id="roomData6"></div>

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var checkIn = "";
            var checkOut = "";
            var today = new Date();
            Date.prototype.addDays = function (days) {
                var dat = new Date(this.valueOf());
                dat.setDate(dat.getDate() + days);
                return dat;
            }

            $('#demo').daterangepicker({
                "startDate": today,
                "endDate": today.addDays(1)
            }, function (start, end, label) {
                console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
                checkIn = start.format('YYYY-MM-DD');
                checkOut = end.format('YYYY-MM-DD');
                var dates = "From " + checkIn + " To " + checkOut;
                $("[name='startDate']").val(dates);
                $("#chckIn").val(checkIn);
                $("#chckOut").val(checkOut);

                //chckIn


            });

        </script>
        <script>
            //$(document).ready(
            //    setInterval(function () {
            //        var roomCount = $('#ddlNoRooms :selected').text();
            //        alert(roomCount);
            //        $("#roomData").load("images.aspx");

            //    }, 30000)
            //);
            $("#ddlNoRooms").change(function () {
                var roomCount = $('#ddlNoRooms :selected').text();
                for (var x = 0; x < 6; x++) {
                    $('#roomData' + (x + 1)).empty();
                }
                for (var i = 0; i < parseInt(roomCount) ; i++) {

                    $('#roomData' + (i + 1)).load("roomSearch.aspx");
                }
            });
            $("#btnSearchHotel").click(function () {
                roomData();
                //alert("added");

            });
            function roomData() {
                //fixed variables
                var searchData = "";
                var roomStr = "";
                var roomsStr;

                var destination = $("#txtDestinations").val();

                var checkIn = $("#chckIn").val();

                var checkOut = $("#chckOut").val();

                var roomsNum = $('#ddlNoRooms :selected').text();

                for (var i = 0; i < parseInt(roomsNum) ; i++) {
                    var txtChldStr = "";
                    var roomID = "#roomData" + (i + 1);
                    var adltCount = $(roomID + " form div #adltNum").val();
                    //var AdltCount = " Room" + (i + 1) + " = " + adltCount + " , ";

                    var txtAdltStr = "<RoomType>" + adltCount + "</RoomType>";

                    var Kids = $(roomID + " form div #childNum").val();
                    var txtChldAge = "";
                    if (parseInt(Kids) > 0) {
                        for (var z = 0; z < Kids; z++) {
                            var KidOrdr = "#childAge" + (z + 1);
                            var kidAge = $(roomID + " form div " + KidOrdr).val();

                            txtChldAge += "<Age>" + kidAge + "</Age>";

                        }
                        txtChldStr = "<Children>" + txtChldAge + "</Children>";
                    }

                    roomStr = roomStr + "<Room>" + txtAdltStr + txtChldStr + "</Room>";

                }
                roomsStr = "<Rooms>" + roomStr + "</Rooms>";


                document.cookie = "searchDataKoky=" + roomsStr;
            }
        </script>

    </form>
    <div class="loaderCont" style="display:none;" ><div class="loader"></div></div>

</body>
</html>
