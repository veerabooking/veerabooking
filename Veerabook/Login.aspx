﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Veera.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login | Veera Booking</title>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    
    <%--update--%> 
   <!--  <link rel="stylesheet" href="assets/css/style.css"> -->
    <link rel="stylesheet" href="assets/css/loader-style.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/signin.css">
    <link rel="shortcut icon" href="assets/ico/minus.png">
</head>
<body>
 <%--ahmaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaad--%> 
    <form id="form1" runat="server">
        <div>
            <div class="container">
                <div class="" id="login-wrapper">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div id="logo-login">
                                <h1>Veera</h1>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="account-box">

                                <form role="form">
                                    <div class="form-group">
                                        <a href="#" class="pull-right label-forgot">Forgot email?</a>
                                        <label for="inputUsernameEmail">Username or email</label>
                                        <input runat="server" type="text" id="inputUsernameEmail" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <a href="#" class="pull-right label-forgot">Forgot password?</a>
                                        <label for="inputPassword">Password</label>
                                        <input runat="server" type="password" id="inputPassword" class="form-control">
                                    </div>
                                    <div class="checkbox pull-left">
                                        <label>
                                            <input type="checkbox">Remember me</label>
                                    </div>
                                    <button class="btn btn btn-primary pull-right" type="submit">
                                        Log In
                                    </button>
                                </form>
                                <a class="forgotLnk" href="index.html"></a>
                                <div class="or-box">
                          
                                    <center><span class="text-center login-with">Login or <b>Sign Up</b></span></center>
                                    
                                </div>
                                <div class="row-block">
                                    <div class="row">
                                        <div class="col-md-12 row-block">
                                            <a href="#" class="btn btn-primary btn-block">Create New Account</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="text-align:center;margin:0 auto;">
                    <h6 style="color:#fff;">Release Candidate 1.0 Powered by © Veera 2018</h6>
                </div>

            </div>
            <div id="test1" class="gmap3"></div>
        </div>
    </form>
</body>
</html>
